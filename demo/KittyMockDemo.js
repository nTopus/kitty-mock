const axios = require("axios");

(async () => {
    let host = 'localhost'
    let serverPort = '6999'
    let url = `http://${host}:${serverPort}`

    //createMocker
    await axios.post(`${url}/create`).then((res) => {
        console.log("Mocker created. Output:\n", res.data)
        return axios.delete(`http://${host}:${JSON.parse(res.data.data).port}/`).then((resDeleteMocker) => {
            console.log("Mocker deleted to release the port. Status:", resDeleteMocker.status, "\n")
        })
    }).catch((error) => {
        console.log(error)
    })

    //createRoute
    await  axios.post(`${url}/create`).then((res) => {
        console.log("Mocker created  to demonstrate how to create a route.\nMocker details:\n", res.data)
        return axios.post(`http://${host}:${JSON.parse(res.data.data).port}/=^.^=/route`, JSON.stringify({
            filters: {path: `/oi`, method: `GET`},
            response: {code: 200, body: `oi`}
        })).then((resRoute) => {
            console.log("Route created. Details:\n", resRoute.data)
            return axios.delete(`http://${host}:${JSON.parse(res.data.data).port}/`).then((resDeleteMocker) => {
                console.log("Mocker deleted to release the port. Status:", resDeleteMocker.status, "\n")
            })
        })
    }).catch((error) => {
        console.log(error)
    })

    //requestRoute
    await  axios.post(`${url}/create`).then((res) => {
        console.log("Mocker created  to demonstrate how to request in a route.\nMocker details:\n", res.data)
        return axios.post(`http://${host}:${JSON.parse(res.data.data).port}/=^.^=/route`, JSON.stringify({
            filters: {path: `/oi`, method: `GET`},
            response: {code: 200, body: `oi`}
        })).then((resRoute) => {
            console.log("Route created. Details:\n", resRoute.data)
            return axios({
                method: `GET`,
                url: `http://${host}:${JSON.parse(res.data.data).port}/oi`
            }).then((resRequestRoute) => {
                console.log("Request route `/oi`. Result:\n", resRequestRoute.data)
                return axios.delete(`http://${host}:${JSON.parse(res.data.data).port}/`).then((resDeleteMocker) => {
                    console.log("Mocker deleted to release the port. Status:", resDeleteMocker.status, "\n")
                })
            })
        })
    }).catch((error) => {
        console.log(error)
    })

    //getHistory
    await axios.post(`${url}/create`).then((res) => {
        console.log("Mocker created  to demonstrate how to get history in a route.\nMocker details:\n", res.data)
        return axios.post(`http://${host}:${JSON.parse(res.data.data).port}/=^.^=/route`, JSON.stringify({
            filters: {path: `/oi`, method: `GET`},
            response: {code: 200, body: `oi`}
        })).then((resRoute) => {
            console.log("Route created:\n", resRoute.data)
            return axios({
                method: `GET`,
                url: `http://${host}:${JSON.parse(res.data.data).port}/oi`
            }).then(() => {
                return axios.get(`http://${host}:${JSON.parse(res.data.data).port}/=^.^=/history?path=/oi&method=GET`).then((resGetHistory) => {
                    console.log("Route history:\n", resGetHistory.data)
                    return axios.delete(`http://${host}:${JSON.parse(res.data.data).port}/`).then((resDeleteMocker) => {
                        console.log("Mocker deleted to release the port. Status:", resDeleteMocker.status, "\n")
                    })
                })
            })
        })
    }).catch((error) => {
        console.log(error)
    })

    //clearHistory
    await axios.post(`${url}/create`).then((res) => {
        console.log("Mocker created  to demonstrate how to clear a route's history.\nMocker details:\n", res.data)
        return axios.post(`http://${host}:${JSON.parse(res.data.data).port}/=^.^=/route`, JSON.stringify({
            filters: {path: `/oi`, method: `GET`},
            response: {code: 200, body: `oi`}
        })).then((resRoute) => {
            console.log("Route created:\n", resRoute.data)
            return axios({
                method: `GET`,
                url: `http://${host}:${JSON.parse(res.data.data).port}/oi`
            }).then(() => {
                return axios.delete(`http://${host}:${JSON.parse(res.data.data).port}/=^.^=/history?path=/oi&method=GET`).then((resClearHistory) => {
                    console.log("Clear history output. \n", resClearHistory.status)
                    return axios.delete(`http://${host}:${JSON.parse(res.data.data).port}/`).then((resDeleteMocker) => {
                        console.log("Mocker deleted to release the port. Status:", resDeleteMocker.status, "\n")
                    })
                })
            })
        })
    }).catch((error) => {
        console.log(error)
    })

    //deleteRoute
    await axios.post(`${url}/create`).then((res) => {
        console.log("Mocker created to demonstrate how to delete a route.\nMocker details:\n", res.data)
        return axios.post(`http://${host}:${JSON.parse(res.data.data).port}/=^.^=/route`, JSON.stringify({
            filters: {path: `/oi`, method: `GET`},
            response: {code: 200, body: `oi`}
        })).then((resRoute) => {
            console.log("Route created:\n", resRoute.data)
            return axios.delete(`http://${host}:${JSON.parse(res.data.data).port}/=^.^=/route?path=/oi&method=GET`).then((resDeleteRoute) => {
                console.log("Deleted route output. \n", resDeleteRoute.status)
                return axios.delete(`http://${host}:${JSON.parse(res.data.data).port}/`).then((resDeleteMocker) => {
                    console.log("Mocker deleted to release the port. Status:", resDeleteMocker.status, "\n")
                })
            })
        })
    }).catch((error) => {
        console.log(error)
    })
})()
