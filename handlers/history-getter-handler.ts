import getJsend from '../helpers/get-jsend'
import IResponse from '../interfaces/IResponse'
import { IncomingMessage } from 'http'
import IRequest from '../interfaces/IRequest'
import IHttpMocker from '../interfaces/IHttpMocker'
import IQueryResponse from '../interfaces/IQueryResponse'
import getAndCheckQuery from '../helpers/get-and-check-query'

/**
 * @api {get} :port/=^.^=/history Get a route history

 * @apiGroup HttpMocker
 * @apiVersion 0.4.3
 * @apiName GetHistory
 * @apiDescription It gets a route history.
 *
 * @apiParam {String} path    The route path. It musts be a query param. If with query, must be encoded.
 * @apiParam {String} method  The route method. It musts be a query param.
 *
 * @apiSuccess {String} status Request status.
 * @apiSuccess {Object} data An array with requests history.
 * @apiSuccess {String} data.ip Remote IP which made the request.
 * @apiSuccess {Object} data.header Request header.
 * @apiSuccess {String} data.header.connection Request connection type.
 * @apiSuccess {String} data.header.contentType Request content type.
 * @apiSuccess {String} data.body Request body.
 * @apiSuccess {String} data.method Request method.
 * @apiSuccess {String} data.url Request path.
 * @apiSuccess {String} data.date Request date.
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "status": "success",
 *       "data": [
 *           {
 *            "ip": "127.0.0.1",
 *            "header": {
 *             "connection": "keep-alive",
 *             "contentType": "application/json"
 *             },
 *            "body": "test",
 *            "method": "GET",
 *            "url": "/oi",
 *            "date": "Wed Jan 29 2020 11:28:02 GMT-0300 (GMT-03:00)"
 *          }
 *       ]
 *    }
 *
 * @apiError {String} status Request status.
 * @apiError {String} message Error message.

 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "status": "fail",
 *       "message": "route does not exist"
 *     }
 */

export default class HistoryGetterHandler {
  private mocker: IHttpMocker

  constructor (mocker: IHttpMocker) {
    this.mocker = mocker
  }

  public handle (req: IncomingMessage): Promise<IResponse> {
    return new Promise((resolve) => {
        let response: IQueryResponse = getAndCheckQuery(req)
        if (response.err)
          return resolve({
            code: 400,
            body: getJsend({ statusCode: 400, data: undefined, message: response.err })
          })
        this.mocker.getRouteShelf().getItem(response.query.path, response.query.method)
          .then(() => {
            let requests: IRequest[] = this.mocker.getRequestShelf().getRequests(response.query.method.toUpperCase() + response.query.path)
            resolve({
              code: 200,
              body: getJsend({ statusCode: 200, data: requests, message: undefined })
            })
          })
          .catch((code: number) => {
            resolve({
              code: code,
              body: getJsend({ statusCode: code, data: undefined, message: 'route does not exist' })
            })
          })
      }
    )
  }

}
