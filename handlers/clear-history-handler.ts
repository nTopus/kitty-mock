import getJsend from '../helpers/get-jsend'
import IResponse from '../interfaces/IResponse'
import { IncomingMessage } from 'http'
import IHttpMocker from '../interfaces/IHttpMocker'
import IQueryResponse from '../interfaces/IQueryResponse'
import getAndCheckQuery from '../helpers/get-and-check-query'

/**
 * @api {delete} :port/=^.^=/history Clear a route history

 * @apiGroup HttpMocker
 * @apiVersion 0.4.3
 * @apiName DeleteRouteHistory
 * @apiDescription It clears a route history.
 *
 * @apiParam {String} path    The route path. It musts be a query param.
 * @apiParam {String} method  The route method. It musts be a query param.
 *
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 204 OK
 */

export default class ClearHistoryHandler {
  private mocker: IHttpMocker

  constructor (mocker: IHttpMocker) {
    this.mocker = mocker
  }

  public handle (req: IncomingMessage): Promise<IResponse> {
    return new Promise((resolve) => {
      let response: IQueryResponse = getAndCheckQuery(req)
      if (response.err)
        return resolve({
          code: 400,
          body: getJsend({ statusCode: 400, data: undefined, message: response.err })
        })
      this.mocker.getRouteShelf().getItem(response.query.path, response.query.method)
        .then(() => {
          this.mocker.getRequestShelf().deleteRequests(response.query.method.toUpperCase() + response.query.path)
          resolve({
            code: 204,
            body: getJsend({ statusCode: 204, data: undefined, message: undefined })
          })
        }).catch((code: number) => {
        resolve({
          code: code,
          body: getJsend({ statusCode: code, data: undefined, message: 'route does not exist' })
        })
      })
    })
  }
}
