import IResponse from '../interfaces/IResponse'
import { IncomingMessage } from 'http'
import getJsend from '../helpers/get-jsend'

/**
 * @api {get} :port/ Check mocker health
 * @apiVersion 0.4.3
 * @apiGroup HttpMocker
 * @apiName CheckHealth
 * @apiDescription It checks mocker health.
 *
 * @apiParam {String} path    The route path. It musts be a query param.
 * @apiParam {String} method  The route method. It musts be a query param.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 204 OK
 */

export default class MockerHealthCheckerHandler {

  public handle (req: IncomingMessage): Promise<IResponse> {
    return new Promise((resolve) => {
      resolve({ code: 204, body: getJsend({ statusCode: 204, data: undefined, message: undefined }) })
    })
  }
}




