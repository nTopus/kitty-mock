import getJsend from '../helpers/get-jsend'
import { IncomingMessage } from 'http'
import HttpMocker from '../mocker/httpMocker'
import { DELETE, GET, PATCH, POST } from '../consts/methods-consts'
import CreateRouteHandler from './create-route-handler'
import DeleteRouteHandler from './delete-route-handler'
import RoutesGetterHandler from './routes-getter-handler'
import MockerHealthCheckerHandler from './mocker-health-checker-handler'
import StopMockerHandler from './stop-mocker-handler'
import IResponse from '../interfaces/IResponse'
import HistoryGetterHandler from './history-getter-handler'
import ClearHistoryHandler from './clear-history-handler'
import getRandomPort from '../helpers/get_port'
import { RESERVED_PATH } from '../consts/kitty'
import isPortFree from '../helpers/IsPortFree'
import CreateWsRouteHandler from './create-ws-route-handler'
import DeleteWsRouteHandler from './delete-ws-route-handler'
import UpdateRouteHandler from './update-route-handler'
import RouteGetterHandler from './route-getter-handler'
import getQueryStringByName from '../helpers/getQueryStringByName'

/**
 * @api {post} /create Create a new HttpMocker

 * @apiGroup HttpMocker
 * @apiVersion 0.4.3
 * @apiName CreateHttpMocker
 * @apiDescription It creates a new HttpMocker.
 *
 * @apiSuccess {String} status Request status.
 * @apiSuccess {Object} data HttpMocker details.
 * @apiSuccess {Number} data.port HttpMocker port.
 * @apiSuccess {String} message Success message.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "status": "success",
 *       "data": {
 *         "port": 0000
 *       }
 *     }
 *
 * @apiError {String} status Request status.
 * @apiError {String} message Error message.

 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *       "status": "error",
 *       "message": "no available port"
 *     }
 */

export default class CreateHttpMockerHandler {
  readonly hostname: string
  readonly portsRange: number[]
  private usedPorts: number[] = []
  private requestedMockerPort: number

  constructor (hostname: string, portsRange: number[], requestedPort?: number) {
    this.hostname = hostname
    this.portsRange = portsRange
    this.requestedMockerPort = requestedPort
  }

  public handle (req: IncomingMessage): Promise<IResponse> {
    if (req) this.requestedMockerPort = Number(getQueryStringByName('port', req.url))
    return this.validatePortStatusAndBeginRequest()
  }

  createHttpMocker (): Promise<IResponse> {
    return new Promise(async (resolve) => {
      let port = this.requestedMockerPort || getRandomPort(this.portsRange, this.usedPorts)
      if (!port) {
        return resolve({
          code: 500,
          body: getJsend({ statusCode: 500, data: undefined, message: 'no available port' })
        })
      }
      const mocker = new HttpMocker(this.hostname, port)
      mocker.loadServer()
      return mocker.runServer().then(() => {
        this.initiateMockerStandardRoutes(mocker)
        this.usedPorts.push(port)
        return resolve({
          code: 200,
          body: getJsend({
            statusCode: 200,
            data: { port },
            message: 'mocker successfully created'
          })
        })
      }).catch(() => {
        if (!this.requestedMockerPort) {
          console.log(`Retrying to connect in another port, ${port} is in use.`)
          this.usedPorts.push(port)
          return resolve(this.createHttpMocker())
        }
        return resolve({
          code: 500,
          body: getJsend({ statusCode: 500, data: undefined, message: 'port is in use' })
        })
      })
    })
  }

  private validatePortStatusAndBeginRequest(): Promise<IResponse> {
    if (this.requestedMockerPort && !isPortFree({
      port: this.requestedMockerPort,
      portsRange: this.portsRange,
      usedPorts: this.usedPorts
    })) {
      return Promise.resolve({
        code: 500,
        body: getJsend({ statusCode: 500, data: undefined, message: 'port is out of range' })
      })
    }
    return this.createHttpMocker()
  }

  private initiateMockerStandardRoutes (mocker: HttpMocker) {
    mocker.addRoute({
      filters: { path: '/', method: GET },
      response: MockerHealthCheckerHandler.prototype.handle.bind(new MockerHealthCheckerHandler())
    })
    mocker.addRoute({
      filters: { path: `/${RESERVED_PATH}/route`, method: GET },
      response: RouteGetterHandler.prototype.handle.bind(new RouteGetterHandler(mocker))
    })
    mocker.addRoute({
      filters: { path: `/${RESERVED_PATH}/routes`, method: GET },
      response: RoutesGetterHandler.prototype.handle.bind(new RoutesGetterHandler(mocker))
    })
    mocker.addRoute({
      filters: { path: `/${RESERVED_PATH}/history`, method: GET },
      response: HistoryGetterHandler.prototype.handle.bind(new HistoryGetterHandler(mocker))
    })
    mocker.addRoute({
      filters: { path: `/${RESERVED_PATH}/route`, method: POST },
      response: CreateRouteHandler.prototype.handle.bind(new CreateRouteHandler(mocker))
    })
    mocker.addRoute({
      filters: { path: `/${RESERVED_PATH}/ws-route`, method: POST },
      response: CreateWsRouteHandler.prototype.handle.bind(new CreateWsRouteHandler(mocker))
    })
    mocker.addRoute({
      filters: { path: `/${RESERVED_PATH}/route`, method: PATCH },
      response: UpdateRouteHandler.prototype.handle.bind(new UpdateRouteHandler(mocker))
    })
    mocker.addRoute({
      filters: { path: '/', method: DELETE },
      response: StopMockerHandler.prototype.handle.bind(new StopMockerHandler(mocker, CreateHttpMockerHandler.prototype.removePort.bind(this)))
    })
    mocker.addRoute({
      filters: { path: `/${RESERVED_PATH}/route`, method: DELETE },
      response: DeleteRouteHandler.prototype.handle.bind(new DeleteRouteHandler(mocker))
    })
    mocker.addRoute({
      filters: { path: `/${RESERVED_PATH}/history`, method: DELETE },
      response: ClearHistoryHandler.prototype.handle.bind(new ClearHistoryHandler(mocker))
    })
    mocker.addRoute({
      filters: { path: `/${RESERVED_PATH}/ws-route`, method: DELETE },
      response: DeleteWsRouteHandler.prototype.handle.bind(new DeleteWsRouteHandler(mocker))
    })
  }

  private removePort (mockerPort: number) {
    this.usedPorts = this.usedPorts.filter(port => {
      return port != mockerPort
    })
  }
}
