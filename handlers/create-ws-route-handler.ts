import getJsend from '../helpers/get-jsend'
import getRequestBody from '../helpers/get-request-body'
import { IncomingMessage } from 'http'
import IRoute from '../interfaces/IRoute'
import IResponse from '../interfaces/IResponse'
import checkCreateWsRouteRequest from '../helpers/check-create-ws-route-request'
import * as WebSocket from 'ws'
import { RESERVED_PATH } from '../consts/kitty'
import IHttpMocker from '../interfaces/IHttpMocker'

/**
 * @api {post} :port/=^.^=/ws-route Create a new websocket route

 * @apiGroup HttpMocker
 * @apiVersion 0.4.3
 * @apiName CreateWSRoute
 * @apiDescription It creates a new websocket route in HttpMocker and automatically a pair called :port/ws-pair/ROUTE_PATH, everything sent in a side will be sent to the other, if there is no connection data will be storage and sent immediately when connected.
 *
 * @apiParam {Object} filters         The route filters.
 * @apiParam {String} filters.path    The route path.
 *
 * @apiSuccess {String} status Request status
 * @apiSuccess {String} message Success message
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "status": "success",
 *       "message": "route successfully created"
 *     }
 *
 * @apiError {String} status Request status.
 * @apiError {String} message Error message.

 * @apiErrorExample Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *        "status": "fail",
 *        "message": "request with invalid route path"
 *     }
 */
export default class CreateWsRouteHandler {
  private mocker: IHttpMocker

  constructor (mocker: IHttpMocker) {
    this.mocker = mocker
  }

  public handle (req: IncomingMessage): Promise<IResponse> {
    return getRequestBody(req).then((body) => {
      let route: IRoute = JSON.parse(body)
      route.response = route.response as IResponse
      let err: string | undefined = checkCreateWsRouteRequest(route.filters, route.response)
      if (err) {
        return { code: 400, body: getJsend({ statusCode: 400, data: undefined, message: err }) }
      }
      let ok: boolean = this.mocker.getWsShelf().setWs({
        routeWs: {
          path: route.filters.path,
          server: new WebSocket.Server({ noServer: true }),
          socket: undefined,
          messages: []
        },
        routePairWs: {
          path: `/${RESERVED_PATH}/ws-pair${route.filters.path}`,
          server: new WebSocket.Server({ noServer: true }),
          socket: undefined,
          messages: []
        }
      })
      if (ok) {
        return {
          code: 200,
          body: getJsend({ statusCode: 200, data: undefined, message: 'route successfully created' })
        }
      } else {
        return {
          code: 400,
          body: getJsend({ statusCode: 400, data: undefined, message: 'route already created in this mocker' })
        }
      }

    }).catch((err) => {
      return {
        code: 400,
        body: getJsend({ statusCode: 400, data: undefined, message: 'request missing body. ' + err.message })
      }
    })
  }
}




