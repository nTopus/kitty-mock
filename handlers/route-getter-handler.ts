import getJsend from '../helpers/get-jsend'
import IResponse from '../interfaces/IResponse'
import { IncomingMessage } from 'http'
import IHttpMocker from '../interfaces/IHttpMocker'
import IQueryResponse from '../interfaces/IQueryResponse'
import getAndCheckQuery from '../helpers/get-and-check-query'

/**
 * @api {get} :port/=^.^=/route Get a route details

 * @apiGroup HttpMocker
 * @apiVersion 0.4.3
 * @apiName GetRoute
 * @apiDescription It gets a route details.
 *
 * @apiParam {String} path    The route path. It musts be a query param. If with query, must be encoded.
 * @apiParam {String} method  The route method. It musts be a query param.
 *
 * @apiSuccess {String} status Request status.
 * @apiSuccess {Object} data An object with route details.
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "status": "success",
 *       "data": {
 *       "filters": {
 *          "path": "/oi",
 *          "method": "GET"
 *     },
 *       "validator": [
 *        {
 *          "matchers": {
 *           "body":"teste"
 *      }
 * },
 *     "code": 200,
 *      "body": "valid"
 *   }
 * }
 *
 * @apiError {String} status Request status.
 * @apiError {String} message Error message.

 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "status": "fail",
 *       "message": "route does not exist"
 *     }
 */

export default class RouteGetterHandler {
  private mocker: IHttpMocker

  constructor (mocker: IHttpMocker) {
    this.mocker = mocker
  }

  public handle (req: IncomingMessage): Promise<IResponse> {
    return new Promise((resolve) => {
        let response: IQueryResponse = getAndCheckQuery(req)
        if (response.err)
          return resolve({
            code: 400,
            body: getJsend({ statusCode: 400, data: undefined, message: response.err })
          })
        this.mocker.getRouteShelf().getItem(response.query.path, response.query.method)
          .then((route) => {
            resolve({
              code: 200,
              body: getJsend({ statusCode: 200, data: route, message: undefined })
            })
          })
          .catch((code: number) => {
            resolve({
              code: code,
              body: getJsend({ statusCode: code, data: undefined, message: 'route does not exist' })
            })
          })
      }
    )
  }

}
