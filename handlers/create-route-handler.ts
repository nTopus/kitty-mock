import getJsend from '../helpers/get-jsend'
import getRequestBody from '../helpers/get-request-body'
import { IncomingMessage } from 'http'
import checkCreateRouteRequest from '../helpers/check-create-route-request'
import IRoute from '../interfaces/IRoute'
import IResponse from '../interfaces/IResponse'
import IHttpMocker from '../interfaces/IHttpMocker'

/**
 * @api {post} :port/=^.^=/route Create a new route

 * @apiGroup HttpMocker
 * @apiVersion 0.4.3
 * @apiName CreateRoute
 * @apiDescription It creates a new route in HttpMocker
 *
 * @apiParam {Object} filters         The route filters.
 * @apiParam {String} filters.path    The route path.
 * @apiParam {String} filters.method  The route method.
 * @apiParam {Object} [response]        The route response. It musts be set if does not exist a validator.
 * @apiParam {Number} response.code   The route response code. It musts be between 100 and 600.
 * @apiParam {String} response.body   The route response body.
 * @apiParam {Object[]} [validator]   The route request validator(s), which checks the request.
 * @apiParam {Object} validator.matchers   The validator request matcher.
 * @apiParam {String||Object} validator.matchers.body   The validator request body matcher.
 * @apiParam {String[String||Object]} validator.matchers.header   The validator request header matcher. A object map with string keys and string values or objects values.
 * @apiParam {String} validator.code   The code response when validator(s) matched.
 * @apiParam {String||Object} validator.body   The body response when validator(s) matched.
 *
 * @apiSuccess {String} status Request status
 * @apiSuccess {String} message Success message
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "status": "success",
 *       "message": "route successfully created"
 *     }
 *
 * @apiError {String} status Request status.
 * @apiError {String} message Error message.

 * @apiErrorExample Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *        "status": "fail",
 *        "message": "request with invalid route path"
 *     }
 */
export default class CreateRouteHandler {
  private mocker: IHttpMocker

  constructor (mocker: IHttpMocker) {
    this.mocker = mocker
  }

  public handle (req: IncomingMessage): Promise<IResponse> {
    return getRequestBody(req).then((body) => {
      let route: IRoute = JSON.parse(body)
      route.response = route.response as IResponse
      let err: string | undefined = checkCreateRouteRequest(route)
      if (err) return { code: 400, body: getJsend({ statusCode: 400, data: undefined, message: err }) }
      if (route.response)
        route.response.body = typeof route.response.body != 'string' ? JSON.stringify(route.response.body) : route.response.body
      if (route.validator)
        for (let i = 0; i < route.validator.length; i++)
          if (route.validator && route.validator[i].matchers.body)
            route.validator[i].matchers.body = typeof route.validator[i].matchers.body != 'string' ? JSON.stringify(route.validator[i].matchers.body) : route.validator[i].matchers.body
      if (this.mocker.getRouteShelf().setItem(route)) {
        return {
          code: 200,
          body: getJsend({ statusCode: 200, data: undefined, message: 'route successfully created' })
        }
      } else {
        return {
          code: 400,
          body: getJsend({ statusCode: 400, data: undefined, message: 'route already created in this mocker' })
        }
      }
    }).catch((err) => {
      return {
        code: 400,
        body: getJsend({ statusCode: 400, data: undefined, message: 'request missing body. ' + err.message })
      }
    })
  }
}




