import IRequest from '../interfaces/IRequest'
import IRequestShelf from '../interfaces/IRequestShelf'
import IRouteHistoryInfo from '../interfaces/IRouteHistoryInfo'

export default class RequestShelf implements IRequestShelf {
  private routeHistoryInfo: IRouteHistoryInfo[] = []

  public getRequests (routeId: string): IRequest[] {
    let routeHistoryInfo: IRouteHistoryInfo = this.findRouteInfoById(this.routeHistoryInfo, routeId)
    if (routeHistoryInfo) {
      return routeHistoryInfo.requestList
    }
    return []
  }

  public setRequest (routeId: string, request: IRequest): void {
    let routeHistoryInfo: IRouteHistoryInfo = this.findRouteInfoById(this.routeHistoryInfo, routeId)
    if (routeHistoryInfo) {
      routeHistoryInfo.requestList.push(request)
      return
    }
    this.routeHistoryInfo.push({ routeId: routeId, requestList: [request] })
  }

  public deleteRequests (routeId: string): void {
    let routeHistoryInfo: IRouteHistoryInfo = this.findRouteInfoById(this.routeHistoryInfo, routeId)
    if (routeHistoryInfo) {
      this.routeHistoryInfo = this.filterRouteInfoDeletingById(this.routeHistoryInfo, routeId)
    }
  }

  private filterRouteInfoDeletingById (routeInfoList: IRouteHistoryInfo[], routeId: string): IRouteHistoryInfo[] {
    return routeInfoList.filter((routeInfo) => routeInfo.routeId != routeId)
  }

  private findRouteInfoById (routeInfoList: IRouteHistoryInfo[], routeId: string): IRouteHistoryInfo {
    return routeInfoList.find((routeInfo) => routeInfo.routeId == routeId)
  }
}

