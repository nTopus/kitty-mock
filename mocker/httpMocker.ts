import IRouteShelf from '../interfaces/IRouteShelf'
import { createServer, IncomingMessage, Server, ServerResponse } from 'http'
import IRoute from '../interfaces/IRoute'
import getJsend from '../helpers/get-jsend'
import IResponse from '../interfaces/IResponse'
import IHttpMocker from '../interfaces/IHttpMocker'
import { performance } from 'perf_hooks'
import IRequestShelf from '../interfaces/IRequestShelf'
import IRequest from '../interfaces/IRequest'
import getRequestBody from '../helpers/get-request-body'
import RouteShelf from '../routeShelf/route-shelf'
import RequestShelf from '../requestShelf/request-shelf'
import IWsShelf from '../interfaces/IWsShelf'
import WsShelf from '../wsShelf/ws-shelf'
import CreateWsRouteHandler from '../handlers/server-upgrade-handler'
import { printRequestsLog } from '../helpers/print-functions'
import passRequestThroughValitador from '../helpers/pass-request-through-valitador'
import { DELETE, OPTIONS } from '../consts/methods-consts'
import IValidator from '../interfaces/IValidator'
import IFilter from '../interfaces/IFilter'
import { CREATE_MOCKER, RESERVED_PATH } from '../consts/kitty'
import IValidateParams from '../interfaces/IValidateParams'
import ErrnoException = NodeJS.ErrnoException

export default class HttpMocker implements IHttpMocker {
    readonly routeShelf: IRouteShelf
    readonly requestShelf: IRequestShelf
    readonly wsShelf: IWsShelf
    private port: number
    private server: Server
    private hostname: string

    constructor(hostname: string, port: number) {
        this.routeShelf = new RouteShelf()
        this.port = port
        this.hostname = hostname
        this.requestShelf = new RequestShelf()
        this.wsShelf = new WsShelf()
    }

    public getRouteShelf(): IRouteShelf {
        return this.routeShelf
    }

    public getWsShelf(): IWsShelf {
        return this.wsShelf
    }

    public getRequestShelf (): IRequestShelf {
        return this.requestShelf
    }

    public getServerInstance (): Server {
        return this.server
    }

    public loadServer (): void {
        this.server = createServer((req, res) => {
            let timeWhenRequestOccurred: number = performance.now()
            let path: string = this.isStandardKittyMethod(req) ? req.url.split('?', 1)[0] : req.url

            this.routeShelf.getItem(path, req.method).then(async route => {
                let expectedResponse: IResponse = route.response as IResponse

                this.hydrateRequest(req).then(request => {
                    this.requestShelf.setRequest(req.method.toUpperCase() + req.url, request)
                })
                if (typeof route.response === 'function') {
                    return this.sendRequisitionToRequestedMethodHandle({
                        handler: route.response,
                        request: req,
                        serverResponse: res,
                        timeWhenRequestOccurred
                    })
                }
                this.responseIfWithoutValidate((!route.validator), {
                    request: req,
                    serverResponse: res,
                    routeResponse: expectedResponse,
                    timeWhenRequestOccurred
                })
                getRequestBody(req).then(body => {
                    this.validateFilters({
                          request: req,
                          serverResponse: res,
                          body: body,
                          route: route,
                          timeWhenRequestOccurred: timeWhenRequestOccurred
                      },
                      route.validator.filter(validator => validator.matchers.body != '*'),)
                }).catch(() => {
                    this.respRequest(res, getJsend({
                        statusCode: 500,
                        data: undefined,
                        message: undefined
                    }), { method: req.method, path: req.url }, timeWhenRequestOccurred, req.socket.localPort, 500)
                })
            }).catch((code) => {
                if (req.method.toLocaleUpperCase() == OPTIONS && code == 405) {
                    this.respRequest(res, undefined, {
                        method: req.method,
                        path: req.url
                    }, timeWhenRequestOccurred, req.socket.localPort, 204)
                }
                if (typeof code !== 'number') {
                    this.respRequest(res, getJsend({
                        statusCode: 500,
                        data: undefined,
                        message: undefined
                    }), { method: req.method, path: req.url }, timeWhenRequestOccurred, req.socket.localPort, 500)
                }
                this.respRequest(res, getJsend({
                    statusCode: code,
                    data: undefined,
                    message: undefined
                }), { method: req.method, path: req.url }, timeWhenRequestOccurred, req.socket.localPort, code)
            })
        })
        this.server.on('upgrade', (request, socket, head) => {
            new CreateWsRouteHandler(this.wsShelf, request, socket, head).handle()
        })
    }

    public runServer(): Promise<string> {
        return new Promise((resolve, reject) => {
            this.server.listen(Number(this.port), this.hostname, () => {
                resolve(`New mocker running at http://${this.hostname}:${this.port}/`)
            })
            this.server.on('error', (e: ErrnoException) => {
                if (e.code === 'EADDRINUSE')
                    reject(`Address ${this.hostname}:${this.port} already in use...`)
            })
        })
    }

    public addRoute (route: IRoute): void {
        console.log('New route added to mocker on port ' + this.port + ' | ' + ' '.repeat(7 - route.filters.method.length) + route.filters.method + ' ' + route.filters.path)
        this.routeShelf.setItem(route)
    }

    public stopServer(): Promise<string | undefined> {
        console.log('Closing mocker on port ' + this.port)
        return new Promise((resolve, reject) =>
            this.server.close(error => {
                  if (error) {
                      reject(error)
                  }
                  resolve(undefined)
                  console.log('Mocker ' + this.port + ' closed!')
              }
            )
        )
    }

    private validateResponse (validateParams: IValidateParams) {
        try {
            if (passRequestThroughValitador(validateParams.body, validateParams.request, validateParams.validator).ok) {
                this.respRequest(validateParams.serverResponse, validateParams.validator.body, {
                    method: validateParams.request.method,
                    path: validateParams.request.url
                }, validateParams.timeWhenRequestOccurred, validateParams.request.socket.localPort, validateParams.validator.code)
            }
        } catch (validatorResponse) {
            this.respRequest(validateParams.serverResponse, validatorResponse.message, {
                method: validateParams.request.method,
                path: validateParams.request.url
            }, validateParams.timeWhenRequestOccurred, validateParams.request.socket.localPort, 500)
        }
    }

    private validateFilters (validateParams: IValidateParams, validatorsWithoutDefault: IValidator[]) {
        for (let i = 0; i < validatorsWithoutDefault.length; i++) {
            this.validateResponse({
                request: validateParams.request,
                serverResponse: validateParams.serverResponse,
                body: validateParams.body,
                validator: validatorsWithoutDefault[i],
                timeWhenRequestOccurred: validateParams.timeWhenRequestOccurred
            })
            this.validatedErrorResponse(i == validatorsWithoutDefault.length - 1,
              validateParams.route.validator.find(validator => validator.matchers.body == '*'), validateParams.request, validateParams.serverResponse, validateParams.timeWhenRequestOccurred)
        }
    }

    private sendRequisitionToRequestedMethodHandle (validateParams: IValidateParams) {
        return validateParams.handler(validateParams.request).then((response: IResponse) => {
            this.respRequest(validateParams.serverResponse, response.body, {
                method: validateParams.request.method,
                path: validateParams.request.url
            }, validateParams.timeWhenRequestOccurred, validateParams.request.socket.localPort, response.code)
        })
    }

    private responseIfWithoutValidate (isValidatorNotExists: boolean, validateParams: IValidateParams) {
        if (isValidatorNotExists) {
            this.respRequest(validateParams.serverResponse, validateParams.routeResponse.body, {
                method: validateParams.request.method,
                path: validateParams.request.url
            }, validateParams.timeWhenRequestOccurred, validateParams.request.socket.localPort, validateParams.routeResponse.code)
        }
    }

    private validatedErrorResponse (isEmpty: boolean, defaultValidator: IValidator, request: IncomingMessage, response: ServerResponse, timeWhenRequestOccurred: number) {
        if (isEmpty) {
            if (!defaultValidator) {
                this.respRequest(response, undefined, {
                    method: request.method,
                    path: request.url
                }, timeWhenRequestOccurred, request.socket.localPort, 404)
            }
            this.respRequest(response, defaultValidator.body, {
                method: request.method,
                path: request.url
            }, timeWhenRequestOccurred, request.socket.localPort, defaultValidator.code)
        }
    }

    private isStandardKittyMethod (request: IncomingMessage): boolean {
        return request.url.includes(CREATE_MOCKER) || request.url.includes(RESERVED_PATH)
    }

    private respRequest (serverResponse: ServerResponse, body: any, serverRequestFilter: IFilter, initialTime: number, port: number, code: number): void {
        if (serverRequestFilter.path == '/' && serverRequestFilter.method == DELETE)
            serverResponse.setHeader('Connection', 'close')
        serverResponse.writeHead(code, {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': '*'
        })
        serverResponse.end(body, () => {
            printRequestsLog(code, serverRequestFilter.method, serverRequestFilter.path, port, initialTime)
        })
    }

    private async hydrateRequest(req: IncomingMessage): Promise<IRequest> {
        return {
            ip: req.socket.remoteAddress,
            header: req.headers,
            body: await getRequestBody(req),
            method: req.method,
            url: req.url,
            date: new Date().toString()
        }
    }
}
