var round2 = (num)=> Math.round(num*100 ) / 100
var wave = (num)=> round2( Math.sin(num/5) * 7 )
var steps = []
var end = 36*4
for (let i=0; i<end; i++) steps.push(i)

console.log(`
body {
  margin: 0;
  padding: 0 15vw 30px 15vw;
  background: #CCC;
  color: #444;
  font-family: Arial, "Liberation Sans", sans-serif;
  font-size: 16px;
}

h1 {
  font-size: 70px;
  font-weight: bold;
  letter-spacing: -5px;
  margin: 0 -15vw;
  padding: 60px 0 60px 15vw;
  background: #AAA;
  color: #FFF;
  text-shadow: 2px 0 0 #FFF, 2px 2px 7px #666;
}
h1:after {
  content: " =^.^=";
  letter-spacing: 0;
  text-shadow: ${
    steps.map((i)=>
      `${i/5}vw ${wave(i)}px 1px hsl(${i*3} 100% 50% / ${Math.round((1-i/end)*100)}%)`
    ).join(',\n    ')
  };
}

h1 + p {
  margin: -60px 0 70px 0;
  font-size: 150%;
  color: #DDD;
  text-shadow: 2px 2px 5px #666;
}

big {
  font-size: 200%;
}
`)
