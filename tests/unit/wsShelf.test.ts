import { expect } from 'chai'
import 'mocha'
import WsShelf from '../../wsShelf/ws-shelf'
import IWs from '../../interfaces/IWs'
import IWsShelf from '../../interfaces/IWsShelf'
import * as WebSocket from 'ws'
import IWsParams from '../../interfaces/IWsParams'

describe('Request Shelf', () => {

  it('inserts and getting item from ws shelf', () => {
    const wsShelf = new WsShelf()
    let ws: IWs = getMockWs()
    wsShelf.setWs(ws)
    getAndCheckWs(wsShelf, JSON.stringify(ws))
  })
  it('gets deleted ws', () => {
    const wsShelf = new WsShelf()
    let ws: IWs = getMockWs()
    wsShelf.setWs(ws)
    getAndCheckWs(wsShelf, JSON.stringify(ws))
    wsShelf.deleteWs('/oi')
    getAndCheckWs(wsShelf, undefined)
  })
  it('gets deleted ws, setting and getting again', () => {
    const wsShelf = new WsShelf()
    let ws: IWs = getMockWs()
    wsShelf.setWs(ws)
    getAndCheckWs(wsShelf, JSON.stringify(ws))
    wsShelf.deleteWs('/oi')
    getAndCheckWs(wsShelf, undefined)
    wsShelf.setWs(ws)
    getAndCheckWs(wsShelf, JSON.stringify(ws))
  })
  it('gets ws from a empty shelf', () => {
    const wsShelf = new WsShelf()
    getAndCheckWs(wsShelf, undefined)
  })

  it('inserts a message to a ws', () => {
    const wsShelf = new WsShelf()
    let ws: IWs = getMockWs()
    wsShelf.setWs(ws)
    ws.routeWs.messages.push('teste')
    getAndCheckWs(wsShelf, JSON.stringify(ws))
  })
  it('removes a message from a ws', () => {
    const wsShelf = new WsShelf()
    let ws: IWs = getMockWs()
    wsShelf.setWs(ws)
    ws.routeWs.messages.push('teste')
    let message: WebSocket.Data = ws.routeWs.messages.shift()
    getAndCheckWs(wsShelf, JSON.stringify(ws))
    expect(message).to.equal('teste')
  })
  it('removes one message from a ws with many', () => {
    const wsShelf = new WsShelf()
    let ws: IWs = getMockWs()
    wsShelf.setWs(ws)
    ws.routeWs.messages.push('teste')
    ws.routeWs.messages.push('teste')
    let message: WebSocket.Data = ws.routeWs.messages.shift()
    getAndCheckWs(wsShelf, JSON.stringify(ws))
    expect(message).to.equal('teste')
  })
  it('try send all pending message', (done) => {
    const wsShelf = new WsShelf()
    let messagesNumber: number = 0
    let server: WebSocket.Server = runServer(8005)
    server.on('listening', () => {
      server.on('connection', (socket) => {
        socket.on('message', (msg) => {
          messagesNumber++
        })
      })
      let client: WebSocket = connectToWs('8005')
      client.on('open', () => {
        let messages: WebSocket.Data[] = ['Oi', 'tudo', 'bem?']
        let wsParams: IWsParams = { path: undefined, server: undefined, socket: client, messages: messages }
        wsShelf.trySendAllPendingMessages(wsParams)
      })
    })
    setTimeout(() => {
      expect(messagesNumber).to.equal(3)
      server.close()
      done()
    }, 100)
  })
  it('try send pending message when there is nothing', (done) => {
    const wsShelf = new WsShelf()
    let messagesNumber: number = 0
    let server: WebSocket.Server = runServer(8006)
    server.on('listening', () => {
      server.on('connection', (socket) => {
        socket.on('message', (msg) => {
          messagesNumber++
        })
      })
      let client: WebSocket = connectToWs('8006')
      client.on('open', () => {
        let wsParams: IWsParams = { path: undefined, server: undefined, socket: client, messages: [] }
        wsShelf.trySendAllPendingMessages(wsParams)
      })
    })
    setTimeout(() => {
      expect(messagesNumber).to.equal(0)
      server.close()
      done()
    }, 100)
  })
  it('try send message or storage when there is connection', (done) => {
    const wsShelf = new WsShelf()
    let messagesNumber: number = 0
    let server: WebSocket.Server = runServer(8007)
    server.on('listening', () => {
      server.on('connection', (socket) => {
        socket.on('message', (msg) => {
          messagesNumber++
        })
      })
      let client: WebSocket = connectToWs('8007')
      client.on('open', () => {
        let wsParams: IWsParams = { path: undefined, server: undefined, socket: client, messages: [] }
        wsShelf.tryToSendMessageOrStorage(wsParams, 'oi')
      })
    })
    setTimeout(() => {
      expect(messagesNumber).to.equal(1)
      server.close()
      done()
    }, 100)
  })
  it('try send message or storage when there is no connection', () => {
    const wsShelf = new WsShelf()
    let wsParams: IWsParams = { path: undefined, server: undefined, socket: undefined, messages: [] }
    wsShelf.tryToSendMessageOrStorage(wsParams, 'oi')
    expect(wsParams.messages.length).to.equal(1)
    expect(wsParams.messages[0]).to.equal('oi')
  })
})

function getMockWs (): IWs {
  return {
    routeWs: { server: undefined, socket: undefined, path: '/oi', messages: [] },
    routePairWs: { server: undefined, socket: undefined, path: '=^.^=/ws-pair/oi', messages: [] },
  }
}

function getAndCheckWs (wsShelf: IWsShelf, expected: string) {
  let ws: IWs = wsShelf.getWs('/oi')
  expect(JSON.stringify(ws)).to.equal(expected)
}

function connectToWs (port: string): WebSocket {
  return new WebSocket(`ws://localhost:${port}/`)
}

function runServer (port: number): WebSocket.Server {
  return new WebSocket.Server({ port: port })
}