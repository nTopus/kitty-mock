import { expect } from 'chai'
import 'mocha'
import IResponse from '../../interfaces/IResponse'
import axios from 'axios'
import { createServer, Server } from 'http'
import IJsend from '../../interfaces/IJsend'
import StopMockerHandler from '../../handlers/stop-mocker-handler'
import HttpMocker from '../../mocker/httpMocker'
import IHttpMocker from '../../interfaces/IHttpMocker'

describe('Stop mocker handler', () => {

  it('stops a mocker', async () => {
    let mocker: IHttpMocker = new HttpMocker('127.0.0.1', 8001)
    mocker.loadServer()
    mocker.runServer()
    let success: number = 0
    let fail: number = 0
    let server: Server = createServer(async (req, res) => {
      new StopMockerHandler(mocker, () => success++).handle(req).then((response) => {
        checkResponse(response, 'success', undefined, undefined, 204)
        success++
      }).finally(() => {
        res.statusCode = 200
        res.end()
      })
    })
    server.listen(7006)
    await axios.get('http://127.0.0.1:7006')
    expect(success).to.equal(2)
    await axios.get('http://127.0.0.1:8001').catch(() => {
      fail++
    })
    expect(fail).to.equal(1)
    server.close()
  })
})

async function checkResponse (response: IResponse, status: string, data: string, message: string, code: number) {
  let jsend: IJsend = JSON.parse(response.body)
  expect(jsend.status).to.equal(status)
  expect(jsend.data).to.equal(data)
  expect(jsend.message).to.equal(message)
  expect(response.code).to.equal(code)
}


