import { expect } from 'chai'
import 'mocha'
import RouteShelf from '../../routeShelf/route-shelf'
import IRoute from '../../interfaces/IRoute'
import IRouteShelf from '../../interfaces/IRouteShelf'
import IResponse from '../../interfaces/IResponse'

describe('Route Shelf', () => {

  it('inserts and getting item from route shelf', () => {
    const routeShelf = new RouteShelf()
    routeShelf.setItem(getMockRoute('/oi'))
    return getAndCheckItem('/oi', 'POST', 200, routeShelf)
  })
  it('inserts repeated item on route shelf', async () => {
    const routeShelf = new RouteShelf()
    routeShelf.setItem(getMockRoute('/oi'))
    routeShelf.setItem(getMockRoute('/oi'))
    await getAndCheckItem('/oi', 'POST', 200, routeShelf)
    expect(routeShelf.removeItem('/oi', 'POST')).to.true
    await getInexistentItem(routeShelf)

  })
  it('gets a deleted item', async () => {
    const routeShelf = new RouteShelf()
    routeShelf.setItem(getMockRoute('/oi'))
    await getAndCheckItem('/oi', 'POST', 200, routeShelf)
    expect(routeShelf.removeItem('/oi', 'POST')).to.true
    await getInexistentItem(routeShelf)
  })
  it('deletes a item between 2', async () => {
    const routeShelf = new RouteShelf()
    routeShelf.setItem(getMockRoute('/oi'))
    routeShelf.setItem(getMockRoute('/oii'))
    await getAndCheckItem('/oi', 'POST', 200, routeShelf)
    expect(routeShelf.removeItem('/oii', 'POST')).to.true
    expect(routeShelf.getItems().length).equal(1)
  })
  it('updates an existent item', async () => {
    const routeShelf = new RouteShelf()
    routeShelf.setItem(getMockRoute('/oi'))
    await getAndCheckItem('/oi', 'POST', 200, routeShelf)
    let route: IRoute = getMockRoute('/oi')
    route.response = route.response as IResponse
    route.response.code = 201
    routeShelf.updateItem('/oi', 'POST', route)
    await getAndCheckItem('/oi', 'POST', 201, routeShelf)
  })
})

function getMockRoute (path: string): IRoute {
  return {
    filters: { path: path, method: 'POST' },
    response: { code: 200, body: 'oioi' }
  }
}

function getAndCheckItem (path: string, method: string, code: number, routeShelf: IRouteShelf) {
  return routeShelf.getItem(path, method).then(async route => {
    expect(route.filters.path).to.equal('/oi')
    expect(route.filters.method).to.equal('POST')
    if (typeof route.response != 'function') {
      expect(route.response.code).to.equal(code)
      expect(route.response.body).to.equal('oioi')
    }
  })
}

async function getInexistentItem (routeShelf: IRouteShelf) {
  let fail: number = 0
  await routeShelf.getItem('/oi', 'POST')
    .catch(code => {
        expect(code).to.equal(404)
        fail++
      }
    )
  expect(fail).to.equal(1)
}