import { expect } from 'chai'
import checkCreateRouteRequest from '../../helpers/check-create-route-request'
import IRoute from '../../interfaces/IRoute'
import { POST } from '../../consts/methods-consts'

describe('Check request', () => {
  it('checks a valid request', () => {
    let request: IRoute = {
      filters: { path: '/53345sd', method: POST },
      validator: [{ matchers: { header: undefined, body: 'oi' }, code: 200, body: '' }]
    }
    expect(checkCreateRouteRequest(request)).to.equal(undefined)
  })
  it('checks a request invalid path', () => {
    let request: IRoute = {
      filters: { path: '53345sd', method: POST },
      response: { code: 200, body: 'json' }
    }
    expect(checkCreateRouteRequest(request)).to.equal('request with invalid route path')
  })
  it('checks a request invalid method', () => {
    let request: IRoute = {
      filters: { path: '/oi', method: 'adasd' },
      response: { code: 200, body: 'json' }
    }
    expect(checkCreateRouteRequest(request)).to.equal('request with invalid route method')
  })
  it('checks a request invalid response code', () => {
    let request: IRoute = {
      filters: { path: '/oi', method: POST },
      response: { code: 50, body: 'json' }
    }
    expect(checkCreateRouteRequest(request)).to.equal('request with invalid route response code')
  })
  it('checks a request with no path', () => {
    let request: IRoute = {
      filters: { path: undefined, method: POST },
      response: { code: 50, body: 'json' }
    }
    expect(checkCreateRouteRequest(request)).to.equal('request missing route path')
  })
  it('checks a request with no method', () => {
    let request: IRoute = {
      filters: { path: '/oi', method: undefined },
      response: { code: 50, body: 'json' }
    }
    expect(checkCreateRouteRequest(request)).to.equal('request missing route method')
  })
  it('checks a request with no response code', () => {
    let request: IRoute = {
      filters: { path: '/oi', method: POST },
      response: { code: undefined, body: 'json' }
    }
    expect(checkCreateRouteRequest(request)).to.equal('request missing route response code')
  })
  it('checks a request invalid validator code', () => {
    let request: IRoute = {
      filters: { path: '/oi', method: POST },
      validator: [{ matchers: { header: undefined, body: 'oi' }, code: 888, body: '' }]
    }
    expect(checkCreateRouteRequest(request)).to.equal('request with invalid route validator code')
  })
  it('checks a request missing validator matchers', () => {
    let request: IRoute = {
      filters: { path: '/oi', method: POST },
      validator: [{ matchers: {}, code: 200, body: '' }]
    }
    expect(checkCreateRouteRequest(request)).to.equal('request missing validator matchers')
  })
  it('checks a request missing validator matchers 2', () => {
    let request: IRoute = {
      filters: { path: '/oi', method: POST },
      validator: [{ matchers: { header: undefined, body: 'oi' }, code: 200, body: '' }, {
        matchers: {},
        code: 200,
        body: ''
      }]
    }
    expect(checkCreateRouteRequest(request)).to.equal('request missing validator matchers')
  })
  it('checks a request missing validator matchers 3', () => {
    let request: IRoute = {
      filters: { path: '/oi', method: POST },
      validator: [{ matchers: { header: undefined, body: 'oi' }, code: 200, body: '' }, {
        matchers: {
          header: undefined,
          body: 'oi'
        }, code: 800, body: ''
      }]
    }
    expect(checkCreateRouteRequest(request)).to.equal('request with invalid route validator code')
  })
})