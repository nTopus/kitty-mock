import 'mocha'
import getRandomPort from '../../helpers/get_port'
import { expect } from 'chai'

describe('Get port', () => {

  it('gets a port with no used ports', () => {
    const portRange: number[] = [6000, 6001, 6002, 6003]
    const usedPorts: number[] = []
    let port: number = getRandomPort(portRange, usedPorts)
    expect(port).to.be.at.least(6000)
    expect(port).to.be.at.most(6003)
  })
  it('gets a port with used ports', () => {
    const portRange: number[] = [6000, 6001, 6002, 6003]
    const usedPorts: number[] = [6000, 6001]
    let port: number = getRandomPort(portRange, usedPorts)
    expect(port >= 6002 && port <= 6003).to.be.true
  })
  it('gets a port with one free port', () => {
    const portRange: number[] = [6000, 6000]
    const usedPorts: number[] = []
    let port: number = getRandomPort(portRange, usedPorts)
    expect(port).to.equal(6000)
  })
  it('gets a port with no free port', () => {
    const portRange: number[] = [6000, 6002]
    const usedPorts: number[] = [6000, 6002]
    let port: number = getRandomPort(portRange, usedPorts)
    expect(port).to.be.undefined
  })
})



