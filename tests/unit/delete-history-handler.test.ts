import { expect } from 'chai'
import 'mocha'
import IResponse from '../../interfaces/IResponse'
import axios from 'axios'
import { createServer, Server } from 'http'
import IJsend from '../../interfaces/IJsend'
import IRequest from '../../interfaces/IRequest'
import ClearHistoryHandler from '../../handlers/clear-history-handler'
import HttpMocker from '../../mocker/httpMocker'
import IRoute from '../../interfaces/IRoute'

describe('Create delete history getter handler', () => {

  it('deletes a history with request', () => {
    const mocker = new HttpMocker('127.0.0.1', 4000)
    mocker.getRouteShelf().setItem(getMockRoute())
    mocker.getRequestShelf().setRequest('GET/oi', getMockRequest())
    let success: number = 0
    let server: Server = createServer(async (req, res) => {
      new ClearHistoryHandler(mocker).handle(req).then((response) => {
        checkResponse(response, 'success', undefined, undefined, 204)
        success++
      })
      res.statusCode = 200
      res.end()
    })
    server.listen(7003)
    return axios.delete('http://127.0.0.1:7003?path=/oi&method=get').finally(() => {
      expect(success).to.equal(1)
      expect(JSON.stringify(mocker.getRequestShelf().getRequests('GET/oi'))).to.equal('[]')
      return server.close()
    })
  })
  it('deletes a history with no request', () => {
    const mocker = new HttpMocker('127.0.0.1', 4000)
    mocker.getRouteShelf().setItem(getMockRoute())
    let success: number = 0
    let server: Server = createServer(async (req, res) => {
      new ClearHistoryHandler(mocker).handle(req).then((response) => {
        checkResponse(response, 'success', undefined, undefined, 204)
        success++
      })
      res.statusCode = 200
      res.end()
    })
    server.listen(7004)
    return axios.delete('http://127.0.0.1:7004?path=/oi&method=get').finally(() => {
      expect(success).to.equal(1)
      expect(JSON.stringify(mocker.getRequestShelf().getRequests('GET/oi'))).to.equal('[]')
      return server.close()
    })
  })
  it('deletes a history with no query', () => {
    const mocker = new HttpMocker('127.0.0.1', 4000)
    let success: number = 0
    let server: Server = createServer(async (req, res) => {
      new ClearHistoryHandler(mocker).handle(req).then((response) => {
        checkResponse(response, 'fail', undefined, 'request query missing path', 400)
        success++
      })
      res.statusCode = 200
      res.end()
    })
    server.listen(7005)
    return axios.delete('http://127.0.0.1:7005').finally(() => {
      expect(success).to.equal(1)
      expect(JSON.stringify(mocker.getRequestShelf().getRequests('GET/oi'))).to.equal('[]')
      return server.close()
    })
  })
})

async function checkResponse (response: IResponse, status: string, data: string, message: string, code: number) {
  let jsend: IJsend = JSON.parse(response.body)
  expect(jsend.status).to.equal(status)
  expect(JSON.stringify(jsend.data)).to.equal(data)
  expect(jsend.message).to.equal(message)
  expect(response.code).to.equal(code)
}

function getMockRequest (): IRequest {
  return {
    ip: '127.0.0.1',
    header: {
      cookie: undefined,
      authorization: undefined,
      connection: undefined,
      contentType: undefined,
      contentLanguage: undefined
    },
    body: 'oi',
    method: 'GET',
    url: '/oi',
    date: 'data'
  }
}

function getMockRoute (): IRoute {
  return {
    filters: { path: '/oi', method: 'GET' },
    response: { code: 200, body: 'oioi' }
  }
}
