import { expect } from 'chai'
import IRoute from '../../interfaces/IRoute'
import { POST } from '../../consts/methods-consts'
import checkUpdateRouteRequest from '../../helpers/check-update-route-request'

describe('Check request', () => {
  it('checks valid request', () => {
    let request: IRoute = {
      validator: [{ matchers: { header: undefined, body: 'oi' }, code: 200, body: '' }]
    }
    expect(checkUpdateRouteRequest(request)).to.equal(undefined)
  })
  it('checks valid request 2', () => {
    let request: IRoute = {
      response: { code: 200, body: 'json' }
    }
    expect(checkUpdateRouteRequest(request)).to.equal(undefined)
  })

  it('checks request invalid response code', () => {
    let request: IRoute = {
      response: { code: 50, body: 'json' }
    }
    expect(checkUpdateRouteRequest(request)).to.equal('request with invalid route response code')
  })

  it('checks request with no response code', () => {
    let request: IRoute = {
      response: { code: undefined, body: 'json' }
    }
    expect(checkUpdateRouteRequest(request)).to.equal('request missing route response code')
  })

  it('checks request invalid validator code', () => {
    let request: IRoute = {
      validator: [{ matchers: { header: undefined, body: 'oi' }, code: 888, body: '' }]
    }
    expect(checkUpdateRouteRequest(request)).to.equal('request with invalid route validator code')
  })

  it('checks request with filter', () => {
    let request: IRoute = {
      filters: { path: '/oi', method: POST },
      response: { code: 200, body: 'json' },
    }
    expect(checkUpdateRouteRequest(request)).to.equal('filters cannot be updated')
  })

  it('checks request missing validator matchers', () => {
    let request: IRoute = {
      validator: [{ matchers: {}, code: 200, body: '' }]
    }
    expect(checkUpdateRouteRequest(request)).to.equal('request missing validator matchers')
  })

  it('checks request missing validator matchers 2', () => {
    let request: IRoute = {
      validator: [{ matchers: { header: undefined, body: 'oi' }, code: 200, body: '' }, {
        matchers: {},
        code: 200,
        body: ''
      }]
    }
    expect(checkUpdateRouteRequest(request)).to.equal('request missing validator matchers')
  })

  it('checks request missing validator matchers 3', () => {
    let request: IRoute = {
      validator: [{ matchers: { header: undefined, body: 'oi' }, code: 200, body: '' }, {
        matchers: {
          header: undefined,
          body: 'oi'
        }, code: 800, body: ''
      }]
    }
    expect(checkUpdateRouteRequest(request)).to.equal('request with invalid route validator code')
  })
})