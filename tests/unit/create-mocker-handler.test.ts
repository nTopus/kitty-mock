import 'mocha'
import { expect } from 'chai'
import IResponse from '../../interfaces/IResponse'
import CreateHttpMockerHandler from '../../handlers/create-http-mocker-handler'
import axios from 'axios'
import IJsend from '../../interfaces/IJsend'

describe('Testing create mocker handler', () => {
  it('creates a mocker', async () => {
    let jsend: IJsend = await getCreateMockerResponse([5006, 5010], 200)
    assertCreateMockerResponse(jsend, 'success', 'mocker successfully created',  isInRange(jsend, 5006, 5011))
    await checkMockerStatusAndDeleteMocker(jsend.data.port)
  })

  it('Creating a mocker using a specific port', async () => {
    let jsend: IJsend = await getCreateMockerResponse([5006, 5010], 200, 5007)
    assertCreateMockerResponse(jsend, 'success', 'mocker successfully created', jsend.data.port == 5007)
    await checkMockerStatusAndDeleteMocker(jsend.data.port)
  })

  it('Should request a mocker two times in the same port sequentially and receive a error message', async () => {
    let jsend: IJsend = await getCreateMockerResponse([5006, 5010], 200, 5007)
    let data = jsend.data
    await checkMockerStatus(data.port)
    jsend = await getCreateMockerResponse([5006, 5010], 500, 5007)
    assertCreateMockerResponse(jsend, 'error', 'port is in use')
    await checkMockerStatusAndDeleteMocker(data.port)
  })

  it('Create three mockers simultaneous in the same port and receive fails in two requests', async () => {
    await Promise.all([
      new CreateHttpMockerHandler('0.0.0.0', [5006, 5010], 5007).handle(undefined),
      new CreateHttpMockerHandler('0.0.0.0', [5006, 5010], 5007).handle(undefined),
      new CreateHttpMockerHandler('0.0.0.0', [5006, 5010], 5007).handle(undefined)
    ]).then((response) => {
      expect(JSON.parse(response[0].body).data.port).to.equal(5007)
      expect(JSON.parse(response[1].body).message).to.equal('port is in use')
      expect(JSON.parse(response[2].body).message).to.equal('port is in use')
      return deleteMocker(JSON.parse(response[0].body).data.port)
    })
  })

  it('Creating a mocker using a specific port but out of range', async () => {
    let jsend: IJsend = await getCreateMockerResponse([5006, 5010], 500, 8000)
    assertCreateMockerResponse(jsend, 'error', 'port is out of range')
  })

  it('Deleting a mocker', async () => {
    let jsend: IJsend = await getCreateMockerResponse([6010, 6014], 200)
    assertCreateMockerResponse(jsend, 'success', 'mocker successfully created')
    expect(jsend.data).not.undefined
    await deleteMocker(jsend.data.port)
  })
})

async function getCreateMockerResponse(portsRange: number[],  expectedCode: number, mockerPort?: number) {
  let response: IResponse = await new CreateHttpMockerHandler('0.0.0.0', portsRange, mockerPort).handle(undefined)
  expect(response.code).to.equal(expectedCode)
  return JSON.parse(response.body)
}

function assertCreateMockerResponse(receivedJsend: IJsend, expectedStatus: string, expectedmessage: string, portCondition?: Boolean): void {
  if (portCondition) {
    expect(portCondition).to.be.true
  }
  expect(receivedJsend.status).to.equal(expectedStatus)
  expect(receivedJsend.message).to.equal(expectedmessage)
}

function isInRange(receivedJsend: IJsend, minPort, maxPort): Boolean {
  return receivedJsend.data.port >= minPort && receivedJsend.data.port < maxPort
}

async function checkMockerStatusAndDeleteMocker(port: string) {
  await checkMockerStatus(port)
  await deleteMocker(port)
}

function deleteMocker (port: string) {
  return axios.delete('http://localhost:' + port + '/').then((response) => {
    expect(response.status).to.equal(204)
  })
}

function checkMockerStatus (port: string) {
  return axios.get('http://localhost:' + port + '/').then((response) => {
    expect(response.status).to.equal(204)
    expect(response.data).to.equal('')
  })
}
