import 'mocha'
import { expect } from 'chai'
import axios, { Method } from 'axios'
import IJsend from '../../interfaces/IJsend'
import IHttpMocker from '../../interfaces/IHttpMocker'
import IRoute from '../../interfaces/IRoute'
import { GET, POST } from '../../consts/methods-consts'
import IRequest from '../../interfaces/IRequest'
import server from '../../server'
import * as WebSocket from 'ws'

describe('Server teste 1', () => {
  let server: IHttpMocker
    before(async () => server = await runServer('127.0.0.1', '4000', '5000-6000'))
    after(async () => await server.stopServer())

  it('checks mocker root functionality', () =>
    createANewMocker('4000', [5000, 6000]).then((port) =>
      checkMockerStatus(port).then(() =>
        deleteMocker(port).then(() =>
          checkDeletedMocker(port)
        )
      )
    )
  )

  it('Create a specific port', async () => {
    await createANewMockerSpecific('4000', '5001').then((port) => {
      expect(port).to.equal('5001')
      checkMockerStatus(port).then(() => {
        deleteMocker(port).then(() =>
          checkDeletedMocker(port)
        )
      })
    })
  })

  it('Request two times in the same specific mocker port and receive success', async () => {
    await createANewMockerSpecific('4000', '5002').then((port) => {
      expect(port).to.equal('5002')
      checkMockerStatus(port)
      return createANewRoute(port, 'route', 'success', 'route successfully created', {
        filters: {path: '/oi', method: 'POST'},
        response: {code: 200, body: 'sddfsdf'}
      }).then(() => {
        requestToARoute('GET', undefined, undefined, {
          port: port,
          path: '/=^.^=/routes',
          expectedCode: 200,
          expectedResponse: '{"status":"success","data":[{"filters":{"path":"/oi","method":"POST"},"response":{"code":200,"body":"sddfsdf"}}]}'
        })
        requestToARoute('GET', undefined, undefined, {
          port: port,
          path: '/=^.^=/routes',
          expectedCode: 200,
          expectedResponse: '{"status":"success","data":[{"filters":{"path":"/oi","method":"POST"},"response":{"code":200,"body":"sddfsdf"}}]}'
        }).finally(() => deleteMocker(port))
      })
    })
  })

  it('Create three mockers simultaneous in the same port and receive fails in two requests', async() => {
    await Promise.all([
      createANewMockerSpecific('4000', '5001'),
      createANewMockerSpecific('4000', '5001'),
      createANewMockerSpecific('4000', '5001')
    ]).then((response) => {
      expect(response[0]).to.equal('5001')
      expect(response[1]).to.equal('port is in use')
      expect(response[2]).to.equal('port is in use')
    })
    deleteMocker('5001').then(() =>
      checkDeletedMocker('5001')
    )
  })

  it('Create a mocker in a specific port and out of range should receive fail', () => {
    createANewMockerSpecific('4000', '6002').then((mockerPort) =>
      expect(mockerPort).to.equal('port is out of range')
    )
  })

  it('checks two mockers in a range with one port', () =>
    runServer('127.0.0.1', '4001', '6000-6001').then((serverMockerRoot) =>
      createANewMocker('4001', [6000, 6001]).then((mockerPort) =>
        createANewMockerWithFail('4001').then(() => {
          deleteMocker(mockerPort).then(() => serverMockerRoot.stopServer())
        })
      )
    )
  )
  it('tests retrying', () =>
      runServer('127.0.0.1', '4002', '4002-4004').then((serverMockerRoot) =>
          createANewMocker('4002', [4000, 4004]).then((mockerPort) =>
                createANewMockerWithFail('4002').then(() => {
                    deleteMocker(mockerPort).then(() => serverMockerRoot.stopServer())
                })
            )
        )
    )
    it('requests to mocker server root with unacceptable methods', () =>
        Promise.all([
            requestToServerRootShouldFail('put'),
            requestToServerRootShouldFail('delete'),
            requestToServerRootShouldFail('get'),
            requestToServerRootShouldFail('patch'),
        ])
    )
    it('requests to mocker server with unacceptable methods', () =>
        createANewMocker('4000', [5000, 6000])
            .then((port) => Promise.all([
                makeRequestToServer('put', port),
                makeRequestToServer('post', port),
                makeRequestToServer('patch', port)
            ]).then(() => deleteMocker(port)))
    )
    it('checks server mocker functionality', () =>
        createANewMocker('4000', [5000, 6000]).then((port) => {
            let route: IRoute = {
                filters: {path: '/oi', method: 'POST'},
                response: {code: 200, body: 'sddfsdf'}
            }
            return createANewRoute(port, 'route', 'success', 'route successfully created', route).then(() =>
                requestToARoute('POST', undefined, undefined, {
                    port: port,
                    path: '/oi',
                    expectedCode: 200,
                    expectedResponse: '"sddfsdf"'
                }).then(() =>
                    deleteARoute(port, {path: '/oi', method: 'POST'}).then(() =>
                        requestToARoute('POST', undefined, undefined, {
                            port: port,
                            path: '/oi',
                            expectedCode: 404,
                            expectedResponse: '{"status":"fail"}'
                        }).then(() => deleteMocker(port))
                    )
                )
            )
        })
    )
    it('checks server mocker routes ', () =>
        createANewMocker('4000', [5000, 6000]).then((port) => {
            let route: IRoute = {
                filters: {path: '/oi', method: 'POST'},
                response: {code: 200, body: 'sddfsdf'}
            }
            return createANewRoute(port, 'route', 'success', 'route successfully created', route).then(() =>
                requestToARoute('GET', undefined, undefined, {
                    port: port,
                    path: '/=^.^=/routes',
                    expectedCode: 200,
                    expectedResponse: '{"status":"success","data":[{"filters":{"path":"/oi","method":"POST"},"response":{"code":200,"body":"sddfsdf"}}]}'
                }).finally(() => deleteMocker(port))
            )
        })
    )
    it('checks server with method OPTIONS and creating a route ', () =>
        createANewMocker('4000', [5000, 6000]).then((port) => {
            requestToARoute('OPTIONS', undefined, undefined, {
                port: port,
                path: '/',
                expectedCode: 204,
                expectedResponse: '""'
            }).then(() => {
                let route: IRoute = {
                    filters: {path: '/oi', method: 'POST'},
                    response: {code: 200, body: 'sddfsdf'}
                }
                return createANewRoute(port, 'route', 'success', 'route successfully created', route).then(() =>
                    requestToARoute('POST', undefined, undefined, {
                        port: port,
                        path: '/oi',
                        expectedCode: 200,
                        expectedResponse: '"sddfsdf"'
                    }).finally(() => deleteMocker(port))
                )
            })

        })
    )
    it('checks server mocker creating repeated routes', () => {
        let route: IRoute = {filters: {path: '/oi', method: 'POST'}, response: {code: 200, body: 'sddfsdf'}}
        return createANewMocker('4000', [5000, 6000]).then((port) =>
            createANewRoute(port, 'route', 'success', 'route successfully created', route).then(() =>
                createANewRoute(port, 'route', 'fail', 'route already created in this mocker', route).finally(() =>
                    deleteMocker(port)
                )
            )
        )
    })
    it('checks server mocker creating route with validator and request valid body', () => {
        let route: IRoute = {
            filters: {path: '/oi', method: 'POST'},
            validator: [{matchers: {header: undefined, body: 'oi'}, code: 200, body: 'valid'}]
        }
        return createANewMocker('4000', [5000, 6000]).then((port) =>
            createANewRoute(port, 'route', 'success', 'route successfully created', route).then(() =>
                requestToARoute('POST', undefined, 'oi', {
                    port: port,
                    path: '/oi',
                    expectedCode: 200,
                    expectedResponse: '"valid"'
                }).finally(() =>
                    deleteMocker(port)
                )
            )
        )
    })
    it('checks server mocker creating route with validator and request with valid header', () => {
        let route: IRoute = {
            filters: {path: '/oi', method: 'POST'},
            validator: [{matchers: {header: {'token': '123'}}, code: 200, body: 'valid'}]
        }
        return createANewMocker('4000', [5000, 6000]).then((port) =>
            createANewRoute(port, 'route', 'success', 'route successfully created', route).then(() =>
                requestToARoute('POST', {'token': '123'}, undefined, {
                    port: port,
                    path: '/oi',
                    expectedCode: 200,
                    expectedResponse: '"valid"'
                }).finally(() =>
                    deleteMocker(port)
                )
            )
        )
    })
    it('checks server mocker creating route with validator and request with valid header 2', () => {
        let route: IRoute = {
            filters: {path: '/oi', method: 'POST'},
            validator: [{matchers: {header: {'token': ['123', '1234']}}, code: 200, body: 'valid'}]
        }
        return createANewMocker('4000', [5000, 6000]).then((port) =>
            createANewRoute(port, 'route', 'success', 'route successfully created', route).then(() =>
                requestToARoute('POST', {'token': '1234'}, undefined, {
                    port: port,
                    path: '/oi',
                    expectedCode: 200,
                    expectedResponse: '"valid"'
                }).finally(() =>
                    deleteMocker(port)
                )
            )
        )
    })
    it('checks server mocker creating route with validator and request with valid header 3', () => {
        let route: IRoute = {
            filters: {path: '/oi', method: 'POST'},
            validator: [{matchers: {header: {'token': '123', 'token2': '1234'}}, code: 200, body: 'valid'}]
        }
        return createANewMocker('4000', [5000, 6000]).then((port) =>
            createANewRoute(port, 'route', 'success', 'route successfully created', route).then(() =>
                requestToARoute('POST', {'token': '123', 'token2': '1234'}, undefined, {
                    port: port,
                    path: '/oi',
                    expectedCode: 200,
                    expectedResponse: '"valid"'
                }).finally(() =>
                    deleteMocker(port)
                )
            )
        )
    })
    it('checks server mocker creating route with validator and request with invalid header', () => {
        let route: IRoute = {
            filters: {path: '/oi', method: 'POST'},
            validator: [{matchers: {header: {'token': '123'}}, code: 200, body: 'valid'}]
        }
        return createANewMocker('4000', [5000, 6000]).then((port) =>
            createANewRoute(port, 'route', 'success', 'route successfully created', route).then(() =>
                requestToARoute('POST', {'token': '1234'}, undefined, {
                    port: port,
                    path: '/oi',
                    expectedCode: 404,
                    expectedResponse: '""'
                }).finally(() =>
                    deleteMocker(port)
                )
            )
        )
    })
    it('checks server mocker creating route with validator which has default response and request valid body', () => {
        let route: IRoute = {
            filters: {path: '/oi', method: 'POST'},
            validator: [{
                matchers: {header: undefined, body: 'oi'},
                code: 200,
                body: 'valid'
            }, {matchers: {header: undefined, body: 'oii'}, code: 201, body: 'vv'}, {
                matchers: {
                    header: undefined,
                    body: '*'
                }, code: 400, body: 'default'
            }]
        }
        return createANewMocker('4000', [5000, 6000]).then((port) =>
            createANewRoute(port, 'route', 'success', 'route successfully created', route).then(() =>
                requestToARoute('POST', undefined, 'teste', {
                    port: port,
                    path: '/oi',
                    expectedCode: 400,
                    expectedResponse: '"default"'
                }).finally(() =>
                    deleteMocker(port)
                )
            )
        )
    })
    it('checks server mocker creating route with validator and valid request body which match with first body matcher', () => {
        let route: IRoute = {
            filters: {path: '/oi', method: 'POST'},
            validator: [{
                matchers: {header: undefined, body: 'oi'},
                code: 200,
                body: 'valid'
            }, {matchers: {header: undefined, body: 'oii'}, code: 201, body: 'valid'}, {
                matchers: {
                    header: undefined,
                    body: '*'
                }, code: 403, body: 'default'
            }]
        }
        return createANewMocker('4000', [5000, 6000]).then((port) =>
            createANewRoute(port, 'route', 'success', 'route successfully created', route).then(() =>
                requestToARoute('POST', undefined, 'oi', {
                    port: port,
                    path: '/oi',
                    expectedCode: 200,
                    expectedResponse: '"valid"'
                }).finally(() =>
                    deleteMocker(port)
                )
            )
        )
    })
    it('checks server mocker creating route with validator and valid request body which match with second body matcher', () => {
        let route: IRoute = {
            filters: {path: '/oi', method: 'POST'},
            validator: [{
                matchers: {header: undefined, body: 'oi'},
                code: 200,
                body: 'valid'
            }, {matchers: {header: undefined, body: 'oii'}, code: 201, body: 'valid'}, {
                matchers: {
                    header: undefined,
                    body: '*'
                }, code: 403, body: 'default'
            }]
        }
        return createANewMocker('4000', [5000, 6000]).then((port) =>
            createANewRoute(port, 'route', 'success', 'route successfully created', route).then(() =>
                requestToARoute('POST', undefined, 'oii', {
                    port: port,
                    path: '/oi',
                    expectedCode: 201,
                    expectedResponse: '"valid"'
                }).finally(() =>
                    deleteMocker(port)
                )
            )
        )
    })
    it('updates an existing route', () => {
        let route: IRoute = {
            filters: {path: '/oi', method: 'POST'},
            response: {code: 200, body: 'test'}
        }
        let updatedRoute: IRoute = {
            response: {code: 201, body: 'test'}
        }
        return createANewMocker('4000', [5000, 6000]).then((port) =>
            createANewRoute(port, 'route', 'success', 'route successfully created', route).then(() =>
                requestToARoute('POST', undefined, updatedRoute, {
                    port: port,
                    path: '/oi',
                    expectedCode: 200,
                    expectedResponse: '"test"'
                }).then(() => {
                    updateRoute(port, {path: '/oi', method: 'post'}, updatedRoute, '""', 204).then(() =>
                        requestToARoute('POST', undefined, updatedRoute, {
                            port: port,
                            path: '/oi',
                            expectedCode: 201,
                            expectedResponse: '"test"'
                        }).finally(() =>
                            deleteMocker(port)
                        )
                    )
                })
            )
        )
    })
    it('updates an existing route, trying to update filters', () => {
        let route: IRoute = {
            filters: {path: '/oi', method: 'POST'},
            response: {code: 200, body: 'test'}
        }
        let updatedRoute: IRoute = {
            filters: {path: '/oii', method: 'POST'},
            response: {code: 201, body: 'test'}
        }
        return createANewMocker('4000', [5000, 6000]).then((port) =>
            createANewRoute(port, 'route', 'success', 'route successfully created', route).then(() =>
                requestToARoute('POST', undefined, updatedRoute, {
                    port: port,
                    path: '/oi',
                    expectedCode: 200,
                    expectedResponse: '"test"'
                }).then(() => {
                    updateRoute(port, {
                        path: '/oi',
                        method: 'post'
                    }, updatedRoute, '{"status":"fail","message":"filters cannot be updated"}', 400).finally(() =>
                        deleteMocker(port)
                    )
                })
            )
        )
    })
    it('updates an existing route, trying to update with invalid data', () => {
        let route: IRoute = {
            filters: {path: '/oi', method: 'POST'},
            response: {code: 200, body: 'test'}
        }
        let updatedRoute: IRoute = {
            response: {code: 888, body: 'test'}
        }
        return createANewMocker('4000', [5000, 6000]).then((port) =>
            createANewRoute(port, 'route', 'success', 'route successfully created', route).then(() =>
                requestToARoute('POST', undefined, updatedRoute, {
                    port: port,
                    path: '/oi',
                    expectedCode: 200,
                    expectedResponse: '"test"'
                }).then(() => {
                    updateRoute(port, {
                        path: '/oi',
                        method: 'post'
                    }, updatedRoute, '{"status":"fail","message":"request with invalid route response code"}', 400).then(() =>
                        deleteMocker(port)
                    )
                })
            )
        )
    })
    it('updates an inexisting route', () => {
        let updatedRoute: IRoute = {
            response: {code: 201, body: 'test'}
        }
        return createANewMocker('4000', [5000, 6000]).then((port) =>
            updateRoute(port, {
                path: '/oi',
                method: 'post'
            }, updatedRoute, '{"status":"fail","message":"route does not exist"}', 404).finally(() =>
                deleteMocker(port)
            )
        )
    })
    it('checks server mocker creating route with two validators and request valid body', () => {
        let route: IRoute = {
            filters: {path: '/oi', method: 'POST'},
            validator: [{
                matchers: {header: undefined, body: 'joao'},
                code: 200,
                body: 'valid joao'
            }, {matchers: {header: undefined, body: 'maria'}, code: 200, body: 'valid maria'}]
        }
        return createANewMocker('4000', [5000, 6000]).then((port) =>
            createANewRoute(port, 'route', 'success', 'route successfully created', route).then(() =>
                requestToARoute('POST', undefined, 'joao', {
                    port: port,
                    path: '/oi',
                    expectedCode: 200,
                    expectedResponse: '"valid joao"'
                }).then(() => {
                    requestToARoute('POST', undefined, 'maria', {
                        port: port,
                        path: '/oi',
                        expectedCode: 200,
                        expectedResponse: '"valid maria"'
                    }).then(() => {
                        requestToARoute('POST', undefined, 'pedro', {
                            port: port,
                            path: '/oi',
                            expectedCode: 404,
                            expectedResponse: '""'
                        }).finally(() =>
                            deleteMocker(port)
                        )
                    })
                })
            )
        )
    })
    it('checks server mocker creating route with validator and request invalid body', () => {
        let route: IRoute = {
            filters: {path: '/oi', method: 'POST'},
            validator: [{matchers: {header: undefined, body: 'oi'}, code: 200, body: 'valid'}]
        }
        return createANewMocker('4000', [5000, 6000]).then((port) =>
            createANewRoute(port, 'route', 'success', 'route successfully created', route).then(() =>
                requestToARoute('POST', undefined, 'oii', {
                    port: port,
                    path: '/oi',
                    expectedCode: 404,
                    expectedResponse: '""'
                }).finally(() =>
                    deleteMocker(port)
                )
            )
        )
    })
    it('checks server mocker creating route with invalid path', () => {
        let route: IRoute = {filters: {path: 'oi', method: 'POST'}, response: {code: 200, body: 'sddfsdf'}}
        return createANewMocker('4000', [5000, 6000]).then((port) =>
            createANewRoute(port, 'route', 'fail', 'request with invalid route path', route).then(() =>
                deleteMocker(port)
            )
        )
    })
    it('checks server mocker creating route with invalid method', () => {
        let route: IRoute = {filters: {path: '/oi', method: 'POsST'}, response: {code: 200, body: 'sddfsdf'}}
        return createANewMocker('4000', [5000, 6000]).then((port) =>
            createANewRoute(port, 'route', 'fail', 'request with invalid route method', route).then(() =>
                deleteMocker(port)
            )
        )
    })
    it('checks server mocker creating route with invalid code', () => {
        let route: IRoute = {filters: {path: '/oi', method: 'POST'}, response: {code: 700, body: 'sddfsdf'}}
        return createANewMocker('4000', [5000, 6000]).then((port) =>
            createANewRoute(port, 'route', 'fail', 'request with invalid route response code', route).then(() =>
                deleteMocker(port)
            )
        )
    })
    it('Check server mocker creating route with invalid json', () => {
        let route: IRoute = {filters: {path: '/oi', method: 'POST'}, response: {code: 700, body: 'sddfsdf'}}
        return createANewMocker('4000', [5000, 6000]).then((port) =>
            tryCreateARouteWithInvalidJson(port, 'fail', 'request missing body. Unexpected end of JSON input').then(() =>
                deleteMocker(port)
            )
        )
    })
    it('checks route history functionality', () =>
        createANewMocker('4000', [5000, 6000]).then((port) => {
            let route: IRoute = {filters: {path: '/oi', method: POST}, response: {code: 200, body: 'sddfsdf'}}
            return createANewRoute(port, 'route', 'success', 'route successfully created', route).then(() =>
                requestToARoute('POST', {"content-type": "application/json"}, undefined, {
                    port: port,
                    path: '/oi',
                    expectedCode: 200,
                    expectedResponse: '"sddfsdf"'
                }).then(() =>
                    getAndCheckRouteHistory(port, {path: '/oi', method: POST}).then(() =>
                        deleteRouteHistory(port, {path: '/oi', method: POST}).then(() =>
                            getAndCheckEmptyRouteHistory(port, {path: '/oi', method: POST}).then(() =>
                                deleteMocker(port)
                            )
                        )
                    )
                )
            )
        })
    )
    it('checks ws-route functionality, sending message from route', () =>
        createANewMocker('4000', [5000, 6000]).then((port) => {
            let route: IRoute = {filters: {path: '/oi', method: POST}, response: {code: 200, body: 'sddfsdf'}}
            return createANewRoute(port, 'ws-route', 'success', 'route successfully created', route).then(() => {
                return new Promise(function (resolve) {
                        const ws1: WebSocket = connectToWs(port, '/oi')
                        ws1.on('open', () => {
                            ws1.on('message', (msg) => {
                                expect(msg).to.equal('oi from pair')
                                deleteMocker(port)
                                ws1.close()
                                ws2.close()
                                resolve()
                            })
                            const ws2: WebSocket = connectToWs(port, '/=%5E.%5E=/ws-pair/oi')
                            ws2.on('open', () => {
                                ws2.on('message', (msg) => {
                                    expect(msg).to.equal('oi from route')
                                    sendMsgToWs(ws2, 'oi from pair')
                                })
                                sendMsgToWs(ws1, 'oi from route')
                            })
                        })
                    }
                )
            })
        })
    )
    it('checks ws-route functionality, sending message from pair', () =>
        createANewMocker('4000', [5000, 6000]).then((port) => {
            let route: IRoute = {filters: {path: '/oi', method: POST}, response: {code: 200, body: 'sddfsdf'}}
            return createANewRoute(port, 'ws-route', 'success', 'route successfully created', route).then(() => {
                return new Promise(function (resolve) {
                        const ws1: WebSocket = connectToWs(port, '/oi')
                        ws1.on('open', () => {
                            ws1.on('message', (msg) => {
                                expect(msg).to.equal('oi from pair')
                                sendMsgToWs(ws1, 'oi from route')
                            })
                            const ws2: WebSocket = connectToWs(port, '/=%5E.%5E=/ws-pair/oi')
                            ws2.on('open', () => {
                                ws2.on('message', (msg) => {
                                    expect(msg).to.equal('oi from route')
                                    deleteMocker(port)
                                    ws1.close()
                                    ws2.close()
                                    resolve()
                                })
                                sendMsgToWs(ws2, 'oi from pair')
                            })
                        })
                    }
                )
            })
        })
    )
    it('checks ws-route functionality, storing messages from route', () =>
        createANewMocker('4000', [5000, 6000]).then((port) => {
            let route: IRoute = {filters: {path: '/oi', method: POST}, response: {code: 200, body: 'sddfsdf'}}
            return createANewRoute(port, 'ws-route', 'success', 'route successfully created', route).then(() => {
                return new Promise(function (resolve) {
                        const ws1: WebSocket = connectToWs(port, '/oi')
                        ws1.on('open', () => {
                            ws1.on('message', (msg) => {
                                expect(msg).to.equal('oi from pair')
                                deleteMocker(port)
                                ws1.close()
                                ws2.close()
                                resolve()
                            })
                            sendMsgToWs(ws1, 'oi from route')
                            const ws2: WebSocket = connectToWs(port, '/=%5E.%5E=/ws-pair/oi')
                            ws2.on('open', () => {
                                ws2.on('message', (msg) => {
                                    expect(msg).to.equal('oi from route')
                                    sendMsgToWs(ws2, 'oi from pair')
                                })
                            })
                        })
                    }
                )
            })
        })
    )
    it('checks ws-route functionality, storing messages from pair', () =>
        createANewMocker('4000', [5000, 6000]).then((port) => {
            let route: IRoute = {filters: {path: '/oi', method: POST}, response: {code: 200, body: 'sddfsdf'}}
            return createANewRoute(port, 'ws-route', 'success', 'route successfully created', route).then(() => {
                return new Promise(function (resolve) {
                        const ws2: WebSocket = connectToWs(port, '/=%5E.%5E=/ws-pair/oi')
                        ws2.on('open', () => {
                            ws2.on('message', (msg) => {
                                expect(msg).to.equal('oi from route')
                                deleteMocker(port)
                                ws1.close()
                                ws2.close()
                                resolve()
                            })
                            sendMsgToWs(ws2, 'oi from pair')
                            const ws1: WebSocket = connectToWs(port, '/oi')
                            ws1.on('open', () => {
                                ws1.on('message', (msg) => {
                                    expect(msg).to.equal('oi from pair')
                                    sendMsgToWs(ws1, 'oi from route')
                                })
                            })
                        })
                    }
                )
            })
        })
    )
    it('checks ws-route functionality, disconnecting and connecting in a pair', () =>
        createANewMocker('4000', [5000, 6000]).then((port) => {
            let route: IRoute = {filters: {path: '/oi', method: POST}, response: {code: 200, body: 'sddfsdf'}}
            return createANewRoute(port, 'ws-route', 'success', 'route successfully created', route).then(() => {
                return new Promise(function (resolve) {
                    const ws1: WebSocket = connectToWs(port, '/oi')
                    ws1.on('open', () => {
                        ws1.on('message', (msg) => {
                            expect(msg).to.equal('oi from pair')
                            deleteMocker(port)
                            ws1.close()
                            ws2.close()
                            resolve()
                        })
                        let ws2: WebSocket = connectToWs(port, '/=%5E.%5E=/ws-pair/oi')
                        ws2.on('open', () => {
                            ws2.close()
                            ws2.on('close', () => {
                                ws2 = connectToWs(port, '/=%5E.%5E=/ws-pair/oi')
                                ws2.on('open', () => {
                                    ws2.on('message', (msg) => {
                                        expect(msg).to.equal('oi from route')
                                        sendMsgToWs(ws2, 'oi from pair')
                                    })
                                    sendMsgToWs(ws1, 'oi from route')
                                })
                            })
                        })
                    })
                })
            })
        })
    )
    it('tries to connect in a non existent ws-route', () =>
        createANewMocker('4000', [5000, 6000]).then((port) => {
            return new Promise(function (resolve) {
                const ws1: WebSocket = connectToWs(port, '/oi')
                ws1.on('error', () => {
                    deleteMocker(port)
                    resolve()
                })
            })
        })
    )
})
describe('Server teste 2', () => {
    let server: IHttpMocker
    before(async () => server = await runServer('127.0.0.1', '4001', '5000-5001'))
    after(() => server.stopServer())

    it('checks if deleted mocker release a port', () =>
        createANewMocker('4001', [5000, 5001]).then((port) =>
            deleteMocker(port).then(() =>
                createANewMocker('4001', [5000, 5001]).then((port2) =>
                    deleteMocker(port2)
                )
            )
        )
    )
    it('checks if deleted mocker is keeping history', () =>
        createANewMocker('4001', [5000, 5001]).then((port) => {
            let route: IRoute = {filters: {path: '/oi', method: POST}, response: {code: 200, body: 'sddfsdf'}}
            return createANewRoute(port, 'route', 'success', 'route successfully created', route).then(() =>
                requestToARoute('POST', {"content-type":"application/json"}, undefined, {
                    port: port,
                    path: '/oi',
                    expectedCode: 200,
                    expectedResponse: '"sddfsdf"'
                }).then(() =>
                    getAndCheckRouteHistory(port, {path: '/oi', method: POST}).then(() =>
                        deleteMocker(port).then(() =>
                            createANewMocker('4001', [5000, 5001]).then((port2) =>
                                createANewRoute(port, 'route', 'success', 'route successfully created', route).then(() =>
                                    getAndCheckEmptyRouteHistory(port, {path: '/oi', method: POST}).then(() =>
                                        deleteMocker(port2)
                                    )
                                )
                            )
                        )
                    )
                )
            )
        })
    )
    it('checks if deleted mocker is keeping routes', () =>
        createANewMocker('4001', [5000, 5001]).then((port) => {
            let route: IRoute = {filters: {path: '/oi', method: POST}, response: {code: 200, body: 'sddfsdf'}}
            return createANewRoute(port, 'route', 'success', 'route successfully created', route).then(() =>
                deleteMocker(port).then(() =>
                    createANewMocker('4001', [5000, 5001]).then((port2) =>
                        requestToARoute('POST', undefined, undefined, {
                            port: port,
                            path: '/oi',
                            expectedCode: 404,
                            expectedResponse: '{"status":"fail"}'
                        }).then(() =>
                            deleteMocker(port2)
                        )
                    )
                )
            )
        })
    )
    it('checks query in route', () =>
        createANewMocker('4001', [5000, 5001]).then((port) => {
            let route: IRoute = {filters: {path: '/oi?teste=1', method: GET}, response: {code: 200, body: 'query'}}
            return createANewRoute(port, 'route', 'success', 'route successfully created', route).then(() =>
                requestToARoute('GET', undefined, undefined, {
                    port: port,
                    path: '/oi?teste=1',
                    expectedCode: 200,
                    expectedResponse: '"query"'
                }).then(() =>
                    deleteMocker(port)
                )
            )
        })
    )

    it('checks query in route 2', () =>
        createANewMocker('4001', [5000, 5001]).then((port) => {
            let route: IRoute = {
                filters: {path: '/oi?teste=2&outro=1', method: GET},
                response: {code: 200, body: 'query 2'}
            }
            return createANewRoute(port, 'route', 'success', 'route successfully created', route).then(() =>
                requestToARoute('GET', undefined, undefined, {
                    port: port,
                    path: '/oi?teste=2&outro=1',
                    expectedCode: 200,
                    expectedResponse: '"query 2"'
                }).then(() =>
                    deleteMocker(port)
                )
            )
        })
    )
    it('checks bad query in route 2', () =>
        createANewMocker('4001', [5000, 5001]).then((port) => {
            let route: IRoute = {
                filters: {path: '/statusSet/get/1?&contractId=1&statusSetItem=true', method: GET},
                response: {code: 502, body: ''}
            }
            return createANewRoute(port, 'route', 'success', 'route successfully created', route).then(() =>
                requestToARoute('GET', undefined, undefined, {
                    port: port,
                    path: '/statusSet/get/1?&contractId=1&statusSetItem=true',
                    expectedCode: 502,
                    expectedResponse: '""'
                }).then(() =>
                    deleteMocker(port)
                )
            )
        })
    )
    it('checks query in route with reserved param', () =>
        createANewMocker('4001', [5000, 6000]).then((port) => {
            let route: IRoute = {
                filters: {path: '/oi?teste=2&method=1', method: POST},
                response: {code: 200, body: 'sddfsdf'}
            }
            return createANewRoute(port, 'route', 'success', 'route successfully created', route).then(() =>
                requestToARoute('POST', {"content-type":"application/json"}, undefined, {
                    port: port,
                    path: '/oi?teste=2&method=1',
                    expectedCode: 200,
                    expectedResponse: '"sddfsdf"'
                }).then(() =>
                    getAndCheckRouteHistory(port, {
                        path: encodeURIComponent('/oi?teste=2&method=1'),
                        method: POST
                    }).then(() =>
                        deleteMocker(port)
                    )
                )
            )
        })
    )

    it('deletes and creating the same route', () =>
        createANewMocker('4001', [5000, 6000]).then((port) => {
            let route: IRoute = {
                filters: {path: '/oi?test=test&test=test', method: GET},
                response: {code: 502, body: ''}
            }
            return createANewRoute(port, 'route', 'success', 'route successfully created', route).then(() =>
                requestToARoute('GET', undefined, undefined, {
                    port: port,
                    path: '/oi?test=test&test=test',
                    expectedCode: 502,
                    expectedResponse: '""'
                }).then(() =>
                    deleteARoute(port,
                        {
                            path: '/oi%3Ftest%3Dtest%26test%3Dtest',
                            method: 'GET'
                        }).then(() =>
                        createANewRoute(port, 'route', 'success', 'route successfully created', route).then(() =>
                            requestToARoute('GET', undefined, undefined, {
                                port: port,
                                path: '/oi?test=test&test=test',
                                expectedCode: 502,
                                expectedResponse: '""'
                            }).then(() =>
                                deleteMocker(port)
                            )
                        )
                    )
                )
            )
        })
    )
})

function runServer(host: string, port: string, portsRange: string): Promise<IHttpMocker> {
    return server({host: host, serverPort: port, mockersPortsRange: portsRange})
}

function requestToServerRootShouldFail(method: Method) {
    return axios({
        method: method,
        url: 'http://localhost:4000/create'
    }).catch((error) => {
        expect(error.response.status).to.equal(405)
    })
}

function makeRequestToServer(method: Method, port: string) {
    return axios({
        method: method,
        url: 'http://localhost:' + port + '/'
    }).catch((error) => {
        expect(error.response.status).to.equal(405)
    })
}

async function createANewMocker(port: string, range: Array<number>): Promise<string> {
    let body: string = ''
    await axios.post(`http://localhost:${port}/create`).then((response) => {
        expect(response.status).to.equal(200)
      body = JSON.stringify(response.data)
    })
    let res: IJsend = JSON.parse(body)
    expect(res.status).to.equal('success')
    expect(res.data.port >= range[0] && range[1] >= res.data.port).to.equal(true)
    expect(res.message).to.equal('mocker successfully created')
    return res.data.port.toString()
}

async function createANewMockerSpecific (hostPort: string, mockerPort: string): Promise<string> {
  return axios.post(`http://localhost:${hostPort}/create?port=${mockerPort}`).then((response) => {
    let res: IJsend = response.data
    return res.data.port.toString()
  }).catch((error) => {
    return (error.response !== undefined) ? error.response.data.message : error.response
  })
}

function deleteARoute(port: string, {path, method}) {
    return axios.delete(`http://localhost:${port}/=^.^=/route?path=${path}&method=${method}`).then((response) => {
        expect(response.status).to.equal(204)
    })

}

function connectToWs(port: string, path: string): WebSocket {
    return new WebSocket(`ws://localhost:${port}${path}`)
}

function sendMsgToWs(ws: WebSocket, msg: string) {
    ws.send(msg)
}

async function getAndCheckRouteHistory(port: string, {path, method}) {
    let body: string = ''
    let success: number = 0
    await axios.get(`http://localhost:${port}/=^.^=/history?path=${path}&method=${method}`).then((response) => {
        expect(response.status).to.equal(200)
        body = JSON.stringify(response.data)
        success++
    })
    expect(success).to.equal(1)
    let res: IJsend = JSON.parse(body)
    expect(res.status).to.equal('success')
    let history: IRequest[] = JSON.parse(JSON.stringify(res.data))
    expect(history[0].ip).to.equal('127.0.0.1')
    expect(history[0].body).to.equal('')
    expect(history[0].header.accept).to.equal("application/json, text/plain, */*")
    expect(history[0].header["content-type"]).to.equal("application/json")
    expect(history[0].header["connection"]).to.equal("close")
    expect(history[0].header["content-length"]).to.equal("0")
    expect(history[0].url).to.equal(decodeURIComponent(path))
    expect(history[0].method).to.equal(POST)
    expect(history[0].date).to.be.not.undefined
}

async function updateRoute(port: string, {path, method}, updatedRoute: IRoute, expectedBody: string, code: number) {
    let body: string = ''
    await axios.patch(`http://localhost:${port}/=^.^=/route?path=${path}&method=${method}`, updatedRoute).then((response) => {
        expect(response.status).to.equal(code)
        body = JSON.stringify(response.data)
    }).catch((err) => {
        body = JSON.stringify(err.response.data)
        expect(err.response.status).to.equal(code)
    })
    expect(body).to.equal(expectedBody)
}

async function getAndCheckEmptyRouteHistory(port: string, {path, method}) {
    let success: number = 0
    await axios.get(`http://localhost:${port}/=^.^=/history?path=${path}&method=${method}`).then((response) => {
        expect(response.status).to.equal(200)
        expect(JSON.stringify(response.data)).to.equal('{"status":"success","data":[]}')
        success++
    })
    expect(success).to.equal(1)
}

async function deleteRouteHistory(port: string, {path, method}) {
    let success: number = 0
    let failed: number = 0
    await axios.delete(`http://localhost:${port}/=^.^=/history?path=${path}&method=${method}`).then((response) => {
        expect(response.status).to.equal(204)
        success++
    }).catch(() => {
        failed++
    })
    expect(failed).to.equal(0)
    expect(success).to.equal(1)
}

async function createANewRoute(port: string, subpath: string, status: string, message: string, route: IRoute) {
    let body: string = ''
    await axios.post('http://localhost:' + port + '/=^.^=/' + subpath, JSON.stringify(route)).then((response) => {
        expect(response.status).to.equal(200)
        body = JSON.stringify(response.data)
    }).catch((error) => {
        body = JSON.stringify(error.response.data)
    })
    let res: IJsend = JSON.parse(body)
    expect(res.status).to.equal(status)
    expect(res.data).to.be.undefined
    expect(res.message).to.equal(message)
}

async function tryCreateARouteWithInvalidJson(port: string, status: string, message: string) {
    let body: string = ''
    await axios.post('http://localhost:' + port + '/=^.^=/route', `{"sdfsdf":100`).then((response) => {
        expect(response.status).to.equal(200)
        body = JSON.stringify(response.data)
    }).catch((error) => {
        body = JSON.stringify(error.response.data)
    })
    let res: IJsend = JSON.parse(body)
    expect(res.status).to.equal(status)
    expect(res.data).to.be.undefined
    expect(res.message).to.equal(message)
}

async function requestToARoute(method: Method, headers: any, data: any, {port, path, expectedCode, expectedResponse}) {
    let body: string = ''
    await axios({
        method: method,
        url: 'http://localhost:' + port + path,
        headers: headers,
        data: data
    }).then((response) => {
        expect(response.status).to.equal(expectedCode)
        body = JSON.stringify(response.data)
    }).catch(err => {
        expect(err.response.status).to.equal(expectedCode)
        body = JSON.stringify(err.response.data)
    })
    expect(body).to.equal(expectedResponse)
}

function createANewMockerWithFail(port: string) {
    return axios.post(`http://localhost:${port}/create`).catch((error) => {
        expect(error.response.status).to.equal(500)
    })
}

function checkMockerStatus(port: string) {
    return axios.get('http://localhost:' + port + '/').then((response) => {
        expect(response.status).to.equal(204)
        expect(response.data).to.equal('')
    })
}

async function checkDeletedMocker(port: string) {
    let failed: number = 0
    await axios.get('http://localhost:' + port + '/').catch(() => {
        failed++
    })
    expect(failed).to.equal(1)
}

function deleteMocker(port: string) {
    return axios.delete('http://localhost:' + port + '/').then((response) => {
        expect(response.status).to.equal(204)
        expect(response.data).to.equal('')
    })
}
