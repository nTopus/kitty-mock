import HttpMocker from './mocker/httpMocker'
import CreateHttpMockerHandler from './handlers/create-http-mocker-handler'
import { POST } from './consts/methods-consts'
import { range } from 'lodash'
import IHttpMocker from './interfaces/IHttpMocker'
import IConfig from './interfaces/IConfig'
import checkParamsConfig from './helpers/check-params-config'

export default function server (config: IConfig): Promise<IHttpMocker> {
  return new Promise((resolve, reject) => {
    let host: string = config.host || '0.0.0.0'
    let port: string = config.serverPort || '4000'
    let rangeArray: string = config.mockersPortsRange || '5000-6000'
    if (config.host != undefined && config.serverPort != undefined && config.mockersPortsRange != undefined) {
      let err: string = checkParamsConfig(config)
      if (err) {
        return reject(err)
      }
    }
    const [portInit, portLimit] = getPortsArray(rangeArray)
    const server = new HttpMocker(host, Number(port))
    server.loadServer()
    server.runServer().then((res) => {
      server.addRoute({
        filters: { path: '/create', method: POST },
        response: CreateHttpMockerHandler.prototype.handle.bind(new CreateHttpMockerHandler(host, range(Number(portInit), Number(portLimit), 1)))
      })
      resolve(server)
    }).catch(reject)
  })
}

function getPortsArray (text: string): Array<string> {
  return text.split('-')
}

