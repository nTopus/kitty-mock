export default function getQueryStringByName(key: string, url: string) {
  if (!url) url = window.location.href;
  key = key.replace(/[\[\]]/g, '\\$&');
  var regex = new RegExp('[?&]' + key + '=([^&#]*)(&|#|$)'), results = regex.exec(url);
  if (!results) return null;
  return decodeURIComponent(results[1].replace(/\+/g, ' '));
}
