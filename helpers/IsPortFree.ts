import IPortValidation from '../interfaces/IPortValidation'

export default function IsPortFree (portValidation: IPortValidation): Boolean{
  portValidation.portsRange.filter((port) => !portValidation.usedPorts.includes(port))
  if (!between(portValidation.port, portValidation.portsRange[0], portValidation.portsRange[portValidation.portsRange.length - 1])) {
    return false
  }
  return true
}

function between(x, min, max) {
  return x >= min && x <= max;
}
