import IQuery from '../interfaces/IQuery'

export function treatWsQuery (query: any): IQuery {
  let treatedQuery: IQuery = { path: '', method: '' }
  switch (typeof query.path) {
    case 'object':
      treatedQuery.path = query.path[0]
      break
    case 'string':
      treatedQuery.path = query.path
  }
  return treatedQuery
}