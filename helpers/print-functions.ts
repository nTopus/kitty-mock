import { KITTY } from '../consts/kitty'
import * as chalk from 'chalk'
import { performance } from 'perf_hooks'

export function printWsLog (date: string, path: string, msg: string | Buffer | ArrayBuffer | Buffer[]): void {
  let repeatSpaceOnPath: number = 25 - path.length
  if (repeatSpaceOnPath < 0) repeatSpaceOnPath = 0
  console.log(`${chalk.yellow.bold(`${KITTY}`)} ${date} | ${chalk.bgBlue.bold(`  ws-message  `)} | ${chalk.gray.bold(`${' '.repeat(repeatSpaceOnPath)}${path}`)} | ${chalk.red.bold(`${msg}`)}`)
}

export function printConnected (date: string, path: string): void {
  console.log(`${chalk.yellow.bold(`${KITTY}`)} ${date} | ${chalk.green.bold(`connection opened on ${path}`)}`)
}

export function printDisconnected (date: string, path: string): void {
  console.log(`${chalk.yellow.bold(`${KITTY}`)} ${date} | ${chalk.red.bold(`connection closed on ${path}`)}`)
}

export function printRequestsLog (code: number, method: string, path: string, port: number, initialTime: number): void {
  let date: string = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '')
  let time: string = (performance.now() - initialTime).toString()
  let repeatSpaceOnTimeExecution: number = 20 - time.length
  let repeatSpaceOnMethod: number = 7 - method.length
  let repeatSpaceOnPort: number = 5 - port.toString().length
  switch (true) {
    case (code >= 500):
      console.log(`${chalk.yellow.bold(`${KITTY}`)} ${date} | ${chalk.bgRed.bold(`  ${code}  `)} | ${' '.repeat(repeatSpaceOnTimeExecution)}${time} ms | ${' '.repeat(repeatSpaceOnPort)}${port} | ${chalk.cyan.bold(`${' '.repeat(repeatSpaceOnMethod)}${method}`)}  ${chalk.blue.bold(`${path}`)}`)
      return
    case (code >= 400 && code < 500):
      console.log(`${chalk.yellow.bold(`${KITTY}`)} ${date} | ${chalk.black.bgYellow.bold(`  ${code}  `)} | ${' '.repeat(repeatSpaceOnTimeExecution)}${time} ms | ${' '.repeat(repeatSpaceOnPort)}${port} | ${chalk.cyan.bold(`${' '.repeat(repeatSpaceOnMethod)}${method}`)}  ${chalk.blue.bold(`${path}`)}`)
      return
    default:
      console.log(`${chalk.yellow.bold(`${KITTY}`)} ${date} | ${chalk.bgGreen.bold(`  ${code}  `)} | ${' '.repeat(repeatSpaceOnTimeExecution)}${time} ms | ${' '.repeat(repeatSpaceOnPort)}${port} | ${chalk.cyan.bold(`${' '.repeat(repeatSpaceOnMethod)}${method}`)}  ${chalk.blue.bold(`${path}`)}`)
  }
}
