import { IncomingMessage } from 'http'
import IQueryResponse from '../interfaces/IQueryResponse'
import { parse } from 'url'
import { checkQuery } from './check-query'
import IQuery from '../interfaces/IQuery'
import { treatQuery } from './treat-query'

export default function getAndCheckQuery (req: IncomingMessage): IQueryResponse {
  let queryObject: any = parse(req.url, true).query
  let err: string | undefined = checkQuery(queryObject)
  if (err) {
    return { err: err, query: undefined }
  }
  let query: IQuery = treatQuery(queryObject)
  query.path = decodeURIComponent(query.path)
  return { err: undefined, query: query }
}
