import IQuery from '../interfaces/IQuery'

export function treatQuery (query: any): IQuery {
  let treatedQuery: IQuery = { path: '', method: '' }
  switch (typeof query.path) {
    case 'object':
      treatedQuery.path = query.path[0]
      break
    case 'string':
      treatedQuery.path = query.path
  }
  switch (typeof query.method) {
    case 'object':
      treatedQuery.method = query.method[0]
      break
    case 'string':
      treatedQuery.method = query.method
  }
  return treatedQuery
}