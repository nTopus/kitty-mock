export const StatusSuccess: string = 'success'
export const StatusError: string = 'error'
export const StatusFail: string = 'fail'