Contributing Guidelines
=======================

1. Clone this repo on your GitLab profile;
2. Clone your GitLab clone to your local machine;
3. `npm install`;
4. Happy Hacking! (with TDD);
5. Commit a consistent change;
6. Goto 4 until you have hacking mojo;
7. Push to your GitLab clone;
8. Make a MR to this repo.
