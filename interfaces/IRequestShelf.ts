import IRequest from './IRequest'

export default interface IRequestShelf {
  getRequests (routeId: string): IRequest[]

  setRequest (routeId: string, request: IRequest): void

  deleteRequests (routeId: string): void

}
