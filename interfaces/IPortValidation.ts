export default interface IPortValidation {
  port: number,
  portsRange: number[],
  usedPorts: number[]
}
