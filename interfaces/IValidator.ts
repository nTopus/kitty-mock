import { IncomingHttpHeaders } from 'http'

export default interface IValidator {
  matchers: Matchers,
  code: number
  body: any
}

interface Matchers {
  header?: IncomingHttpHeaders
  body?: any
}
