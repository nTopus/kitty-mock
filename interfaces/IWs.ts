import IWsParams from './IWsParams'

export default interface IWs {
  routeWs: IWsParams
  routePairWs: IWsParams
}

