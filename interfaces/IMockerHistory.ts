import IRouteHistoryInfo from './IRouteHistoryInfo'

export default interface IMockerHistory {
  mockerPort: string
  routeInfo: IRouteHistoryInfo[]
}


