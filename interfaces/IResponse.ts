export default interface IResponse {
  code: number
  body: any
}
