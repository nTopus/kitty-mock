import IWs from './IWs'
import * as WebSocket from 'ws'
import IWsParams from './IWsParams'

export default interface IWsShelf {
  getWs (path: string): IWs

  setWs (webSocket: IWs): boolean

  deleteWs (path: string): boolean

  trySendAllPendingMessages (ws: IWsParams): void

  tryToSendMessageOrStorage (ws: IWsParams, msg: WebSocket.Data)
}
