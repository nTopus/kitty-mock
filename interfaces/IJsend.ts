export default interface IJsend {
  status: string
  data: any | undefined
  message: string | undefined
}