import IHandler from './IHandler'
import IFilter from './IFilter'
import IResponse from './IResponse'
import IValidator from './IValidator'

export default interface IRoute {
  filters?: IFilter,
  response?: IResponse | IHandler
  validator?: IValidator[]
}
