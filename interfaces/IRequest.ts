import { IncomingHttpHeaders } from 'http'

export default interface IRequest {
  ip: string
  body: string
  header: IncomingHttpHeaders
  url: string
  method: string
  date: string
}