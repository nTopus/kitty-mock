import IQuery from './IQuery'

export default interface IQueryResponse {
  err: string,
  query: IQuery
}