import { IncomingMessage, ServerResponse } from 'http'
import IValidator from './IValidator'
import IRoute from './IRoute'
import IHandler from './IHandler'
import IResponse from './IResponse'

export default interface IValidateParams {
  request?: IncomingMessage,
  serverResponse?: ServerResponse,
  routeResponse?: IResponse,
  body?: any,
  route?: IRoute
  handler?: IHandler
  validator?: IValidator,
  timeWhenRequestOccurred?: number
}
